<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model	{


	public function expenses(){
		return $this->hasMany('App\Expense', 'payment_id');
	}
	public function purchases(){
		return $this->hasMany('App\Purchase', 'payment_id');
	}
	public function sales(){
		return $this->hasMany('App\Sale', 'payment_id');
	}
}