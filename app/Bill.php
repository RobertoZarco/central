<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model	{
	public function bank(){
		return $this->belongsTo('App\Bank');
	}
	public function user(){
		return $this->belongsTo('App\User');
	}
	public function client(){
		return $this->belongsTo('App\Client');
	}
}
