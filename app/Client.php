<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model	{
	use SoftDeletes;

	protected $fillable = [
		'alias', 'reason', 'phone', 'email', 'rfc',
	];
	protected $dates = [
		'deleted_at',
	];

	public function bills(){
		return $this->hasMany('App\Bill');
	}
	public function credits(){
		return $this->hasMany('App\Credit');
	}
	public function sales(){
		return $this->hasMany('App\Sale');
	}
	public function purchases(){
		return $this->hasMany('App\Purchase', 'provider_id');
	}
}
