<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends Model	{
	use SoftDeletes;

	protected $fillable = [
		'name', 'amount', 'purchasePrice', 'salePrice', 'unit_id', 'category_id',
	];

	protected $dates = [
		'deleted_at',
	];

	public function unitType(){
		return $this->belongsTo('App\TypeOfUnit','unit_id','id');
	}
	public function category(){
		return $this->belongsTo('App\Category','category_id','id');
	}
	public function purchases(){
		return $this->hasMany('App\InventoryPurchase');
	}
	public function sales(){
		return $this->hasMany('App\InventorySale');
	}
}
