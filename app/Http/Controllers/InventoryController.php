<?php

namespace App\Http\Controllers;

use App\Category;
use App\Inventory;
use App\TypeOfUnit;
use Jenssegers\Date\Date;
use Illuminate\Http\Request;

class InventoryController extends Controller	{
	public function __construct()	{
		$this->middleware('can:handleInventory');
	}
	public function index()	{
		$inventory = Inventory::all();
		return view('inventories.index', compact("inventory"));
	}

	public function create(){
		return view('inventories.create')->with('categories', Category::all())->with('typesOfUnit', TypeOfUnit::all());
	}

	public function store(Request $request)	{
		$request->validate([
			'name'					=> 'required|string|regex:/^[a-záéíóúüñ\'\ç\s]+$/iu|max:190',
			'amount'				=> 'required|integer|min:0',
			'purchasePrice'	=> 'required|numeric|min:0.01',
			'salePrice'			=> 'required|numeric|min:0.01',
			'unit_id'				=> 'required|integer|exists:type_of_units,id',
			'category_id'		=> 'required|integer|exists:categories,id',
		]);
		$data = $request->all();
		$data['name'] = mb_strtoupper($data['name']);
		$inventory = Inventory::create($data);
		$inventory->name = ucwords(mb_strtolower($inventory->name));
		return redirect()->route('inventories.index')->with('success',"Éxito al <u>crear</u> el producto <strong>$inventory->name</strong><br>".Date::now()->format('h:i:s a.'));
	}

	public function show(Inventory $inventory)	{
		return view('inventories.edit', compact('inventory'))->with('categories', Category::all())->with('typesOfUnit', TypeOfUnit::all());
	}

	public function edit(Inventory $inventory)	{
		return $this->show($inventory);
	}

	public function update(Request $request, Inventory $inventory)	{
		$request->validate([
			'name'					=> 'required|string|regex:/^[a-záéíóúüñ\'\ç\s]+$/iu|max:190',
			'amount'				=> 'required|integer|min:0',
			'purchasePrice'	=> 'required|numeric|min:0.01',
			'salePrice'			=> 'required|numeric|min:0.01',
			'unit_id'				=> 'required|integer|exists:type_of_units,id',
			'category_id'		=> 'required|integer|exists:categories,id',
		]);
		$data = $request->all();
		$data['name'] = mb_strtoupper($data['name']);
		$inventory->fill($data);
		$inventory->save();
		$inventory->name = ucwords(mb_strtolower($inventory->name));
		return redirect()->route('inventories.show', $inventory->id)->with('success',"Éxito al <u>actualizar</u> el producto <strong>$inventory->name</strong><br>".Date::now()->format('h:i:s a.'));
	}

	public function destroy(Inventory $inventory)	{
		$inventory->delete();
		$inventory->name = ucwords(mb_strtolower($inventory->name));
		return redirect()->route('inventories.index')->with('info',"Éxito al <u>eliminar</u> el producto <strong>$inventory->name</strong><br>".Date::now()->format('h:i:s a.'));
	}

	public function getInventoryById($id){
		$inventario = Inventory::findOrFail($id);
		return $inventario;
	}

}