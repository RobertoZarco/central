<?php

namespace App\Http\Controllers;

use App\Client;
use App\PaymentType;
use App\Inventory;
use App\InventoryPurchase;
use App\Purchase;
use Illuminate\Http\Request;
use Mpdf\Mpdf;

class ShopController extends Controller	{
	public function __construct()	{
		$this->middleware('can:handleBuys');
	}
	public function index()	{
		return view('shop.index')->with('clients', Client::all())->with('payments', PaymentType::all());
	}
	public function save(Request $request){
		$provider			= Client::findOrFail($request->get('provider'));
		$paymentType	    = PaymentType::findOrFail($request->get('paymentType'));
		$discount			= $request->get('discount');
		$products			= array();
		foreach ($request->get('products') as $key => $value)
			if($value != null){
				$products[$key] = $value;
			}
		$purchase = new Purchase;
		$purchase->discount = $discount;
		$purchase->provider()->associate($provider);
		$purchase->payment()->associate($paymentType);
		$purchase->save();
		foreach($products as $product => $amount){
			$inventory =  Inventory::findOrFail($product);
			$purchase->inventories()->create([
				'amount'				=> $amount,
				'purchasePrice'	=> $inventory->purchasePrice,
				'inventory_id'	=> $product,
			]);
			$inventory->increment('amount', $amount);
		}
		return response()->json(array('message' => '<div class="alert alert-success" role="alert">Éxito al guardar la compra, procediendo a generar ticket...</div>', 'purchase_id' => encrypt($purchase->id)));
	}
	public function ticket($purchase){
		$purchase = Purchase::findOrFail(decrypt($purchase));
		$subtotal = 0;
		foreach($purchase->inventories as $product)
			$subtotal += $product->purchasePrice*$product->amount;
		// dd($purchase->toArray());
		$mpdf = new Mpdf([
			'mode' => 'utf-8', 
			'format' => 'B',
		]);
		$mpdf->SetTitle("Ticket de compra: ".str_pad($purchase->id, 9, '0', STR_PAD_LEFT));
		$mpdf->SetAuthor('VS');
		$stylesheet = file_get_contents(asset('/css/ticket.css'));
		$mpdf->WriteHTML($stylesheet, 1);
		$mpdf->WriteHTML(view('shop.ticket', compact('purchase', 'subtotal')), 2);
		$mpdf->Output("ticketCompra".str_pad($purchase->id, 9, '0', STR_PAD_LEFT).".pdf", 'I');
		// return view('shop.ticket', compact('purchase'));
	}
	public function filter(Request $request){
		$search = mb_strtoupper($request->get('query'));
		$items = Inventory::where('name', 'like', "%$search%")->get();
		return view('shop.productList', compact('items'));
	}
	public function addItem(Inventory $inventory){
		$inventory->name					= ucfirst(mb_strtolower($inventory->name));
		$inventory->unit_name			= ucfirst(mb_strtolower($inventory->unitType->key));
		$inventory->category_name	= ucfirst(mb_strtolower($inventory->category->key));
		$inventory->salePrice			= number_format($inventory->salePrice, 2);
		$inventory->actions				= $this->generateActions();
		return response()->json($inventory->toArray());
	}
	private function generateActions(){
		return "
			<div class='btn-group-vertical btn-group-sm' role='group' aria-label='...'>
				<button class='btn btn-primary addAmount'><i class='fas fa-sort-up'></i></button>
				<button class='btn btn-primary substractAmount'><i class='fas fa-sort-down'></i></button>
			</div>
			<button class='btn btn-danger deleteItem'><i class='fas fa-trash'></i></button>
		";
	}
}
