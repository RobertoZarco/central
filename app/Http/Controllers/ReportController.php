<?php

namespace App\Http\Controllers;

use App\User;
use App\Client;
use App\Inventory;
use App\InventorySale;
use App\Sale;
use App\InventoryPurchase;
use App\Expense;
use App\Credit;
use App\TypeOfUnit;
use App\Purchase;

use Illuminate\Http\Request;

class ReportController extends Controller	{

	public function __construct(){
		$this->middleware('can:handleReports');
	}

	public function showindex()	{
		$expenses          = Expense::sum('amount');
		$InventorySales    = InventorySale::sum('saleprice'); 
		$credits           = Credit::sum('amount');
		$InventoryPurchase = InventoryPurchase::sum('purchasePrice');


		$data = (object) array(
			"gastos"   => $expenses,
			"ventas"   => $InventorySales,
			"creditos" => $credits,
			"compras"  => $InventoryPurchase,
		);

		return view('reports.index',compact("data"));
	}

	public function showformSales()	{
		$users     = User::all();
		$clients   = Client::all();
		return view('reports.formSales', compact("clients","users"));
	}

	public function showFormShops()	{
		$users     = User::all();
		$clients   = Client::all();
		return view('reports.formShops', compact("clients","users"));
	}

	public function showFormExpenses()	{
		//$expensescharts  = Expense::all('amount')
		//->sum('amount')
		//->groupBy(function($val) {
		//	return \Carbon\Carbon::parse($val->created_at)->format('M');
		//});

		//$suma = 0;

		//dd($expensescharts);

		return view('reports.formExpenses');
	}

	public function showFormPayments()	{
		$users     = User::all();
		$clients   = Client::all();
		return view('reports.formPayments', compact("clients","users"));
	}

	public function showQuerySales(Request $request) {
		
		$fecha1 = $request->input('fecha1');
		$fecha2 = $request->input('fecha2');
		$sales  = Sale::select()
		->whereDate('created_at','>=',$fecha1)
		->whereDate('created_at','<=',$fecha2)
		->where('expire_at',null)
		->get();

		return view('reports.tableQuerySales',compact("sales"));
	}

	public function showQueryExpenses(Request $request) {
		
		$fecha1 = $request->input('fecha1');
		$fecha2 = $request->input('fecha2');
		$expenses  = Expense::select()
		->whereDate('created_at','>=',$fecha1)
		->whereDate('created_at','<=',$fecha2)
		->get();
		
		return view('reports.tableQueryExpenses',compact("expenses"));
	}

	public function showQueryPayments(Request $request) {

	}

	public function showQueryShops(Request $request) {

		$fecha1 = $request->input('fecha1');
		$fecha2 = $request->input('fecha2');

		$shops  = Purchase::select()
		->whereDate('created_at','>=',$fecha1)
		->whereDate('created_at','<=',$fecha2)
		->get();

		//dd($shops);

		return view('reports.tableQueryShops',compact("shops"));
	}

}
