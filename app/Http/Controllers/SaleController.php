<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sale;
use App\Inventory;
use App\Client;
use App\PaymentType;
use App\InventorySale;
use App\Credit;
use Mpdf\Mpdf;

class SaleController extends Controller	{
	public function __construct()	{
		$this->middleware('can:handleSales');
	}

	public function index()	{
		$sales          = Sale::all();
		$inventories    = Inventory::all();
		$clients        = Client::all();
		$paymentTypes   = PaymentType::all();
		$inventorySales = InventorySale::all();

		return view('sales.index', compact("sales", "inventories", "clients", "paymentTypes", "inventorySales"));
	}

	public function save(Request $request){

		if($request->get('descuento')==null){
			$discount = 0;
		}else{
			$discount = $request->get('descuento');
		}

		if($request->get('rebaja')==null){
			$rebaja = 0;
		}else{
			$rebaja = $request->get('rebaja');
		}

		$expire_at  = $request->get('vencimiento');
		$client_id  = Client::findOrFail($request->get('client_id'));
		$payment_id = PaymentType::findOrFail($request->get('payment_id'));
		$venta      = $request->get('venta');
		$cotizacion = $request->get('cotizacion');
		$productos  = $request->get('productos');
		$subTotal   = $request->get('subTotal');
		$total      = $request->get('total');

		//venta
		$sale = new Sale;
		$sale->discount = $rebaja;

		if( is_null( $expire_at)){
			$expire_at = null;
		}

		$sale->expire_at = $expire_at;
		$sale->client()->associate($client_id);
		$sale->payment()->associate($payment_id);
		$sale->save();

		//si la venta es de tipo: pago con credito, se registra adeudo, se almacena en la tabla creditos
		if ($payment_id->id == 6) {
		   $creditsale = new Credit;
		   $creditsale->amount = $total;
		   //se registra en 0 , es decir con un pago pendiente
		   $creditsale->paid = 0;
		   //la fecha de liquidacion esta en vacia
		   $creditsale->paid_at = null;
		   $creditsale->client_id = $client_id->id;
		   $creditsale->user_id = auth()->user()->id;
		   //se salva en la tabla
		   $creditsale->save();
		}


		//inventario de venta
		foreach ($productos as $indice => $amount) {
			if( !is_null($amount) ){
				$sale_id   = $sale->id;
				$inventory = Inventory::findOrFail($indice);

				$sale->inventories()->create([
					'salePrice'	   => $inventory->salePrice,
					'amount'       => $amount,
					'inventory_id' => $indice,
					'sale_id'      => $sale_id,
				]);

				if($venta == 1){
					$inventory->decrement('amount', $amount);
				}
			}
		}

		return response()->json(array('message' => 'Venta guardada correctamente',
																	'sale_id' => encrypt($sale->id)
																	)
														);
	}

	public function ticket($id){
		$sale = Sale::findOrFail( decrypt($id));
		$subTotal = 0;
		foreach ($sale->inventories as $product) {
			$subTotal += $product->salePrice * $product->amount;
		}

		$mpdf = new Mpdf([
			'mode' => 'utf-8',
			'format' => 'B'
		]);

		$mpdf->setTitle('Ticket de venta: '.str_pad($sale->id, 9, 0, STR_PAD_LEFT));
		$mpdf->setAuthor('RH');
		$stylesheet = file_get_contents(asset('/css/ticket.css'));
		$mpdf->WriteHTML($stylesheet, 1);
		$mpdf->WriteHTML(view('sales.ticket', compact('sale','subTotal')),2);
		$mpdf->output('ticketVenta'.str_pad($sale->id, 9,0,STR_PAD_LEFT).".pdf", 'I');
	}

	public function showSalesQuotationDetail(){

		$sales = Sale::select('id,')
		->join('InventorySale', 'sale.id','InventorySale.id');

		//dd($sales);


		return view('sales.showDetail',compact('sales'));
	}

	public function sale($id){
		$sale = Sale::findOrFail($id);
		return view('wastages.edit', compact('sale')) ;
	}



}
