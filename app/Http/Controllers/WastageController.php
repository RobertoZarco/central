<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wastage;
use App\Sale;
use App\Client;
use App\PaymentType;
use App\Inventory;
use App\InventorySale;
use App\InventoryWastages;
use Mpdf\Mpdf;

class WastageController extends Controller
{
	public function __construct(){
		$this->middleware('can:handleWastages');
	}

    public function index(){

      $sales    = Sale::all();
      $wastages = Wastage::all();

      return view('wastages.index', compact("wastages","sales"));
    }

    public function create(Request $request){

      $saleId      = $request->get('saleId');
      $amount      = $request->get('amount');
      $subTotal    = $request->get('subTotal');
      $rebaja      = $request->get('rebaja');
      $descuento   = $request->get('descuento');
      $total       = $request->get('total');
      $productos   = $request->get('productos');
      $devolucion  = $request->get('wastages');
      $client_id   = Client::findOrFail($request->get('client_id'));
      $payment_id  = PaymentType::findOrFail($request->get('payment_id'));
      $description = null;

      //devolucion/merma
      $wastage = new Wastage;
      $wastage->description;
      $wastage->client()->associate($client_id);
      $wastage->sale()->associate($saleId);
      $wastage->save();

      //inventario de venta
      foreach ($productos as $indice => $productAmount) {
        if(!is_null($productAmount)){
          $inventory = Inventory::findOrFail($indice);
          $amount = $productAmount;

          //1 es devolucion
          //0 es merma
          foreach ($devolucion as $key => $dev) {

            if(!is_null($dev)){
              $inventory = Inventory::findOrFail($key);
              $sale = Sale::find($saleId);

              $wastage->inventoryWastages()->create([
                "amount"       => $productAmount,
                "salePrice"    => $inventory->salePrice,
                "lost"         => $dev,
                "description"  => null,
                "wastage_id"   => $wastage->id,
                "inventory_id" => $indice
              ]);

              //devolucion 0 es merma, 1 es devolucion
              if($dev == 1){
                $inventory->increment('amount', $amount);

                $inventoriesSales = InventorySale::where('sale_id', $saleId)
                                    ->where('inventory_id', $indice)
                                    ->decrement('amount', $amount);

              }else{
                $inventoriesSales = InventorySale::where('sale_id', $saleId)
                                    ->where('inventory_id', $indice)
                                    ->decrement('amount', $amount);
                //$inventoriesSales->decrement('amount', $amount);
              }
            }
          }

          return response()->json(array ( 'message' => 'Devolución/Merma guardada correctamente',
                                          'wastage_id' => encrypt($wastage->id)
                                        )
                        );

        }

      }

    }

    public function ticket($id){
      $wastage = Wastage::findOrFail( decrypt($id));
      $subTotal = 0;

      foreach ($wastage->inventories as $product) {
        $subTotal += $product->salePrice * $product->amount;
      }

      $mpdf = new Mpdf([
        'mode' => 'utf-8',
        'format' => 'B'
      ]);

      $mpdf->setTitle('Ticket de venta: '.str_pad($wastage->id, 9, 0, STR_PAD_LEFT));
      $mpdf->setAuthor('RH');
      $stylesheet = file_get_contents(asset('/css/ticket.css'));
      $mpdf->WriteHTML($stylesheet, 1);
      $mpdf->WriteHTML(view('wastages.ticket', compact('wastage','subTotal')),2);
      $mpdf->output('ticketVenta'.str_pad($wastage->id, 9,0,STR_PAD_LEFT).".pdf", 'I');
    }


}
