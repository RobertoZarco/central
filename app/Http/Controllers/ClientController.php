<?php

namespace App\Http\Controllers;

use App\Client;
use Jenssegers\Date\Date;
use Illuminate\Http\Request;

class ClientController extends Controller	{
	public function __construct()	{
		$this->middleware('can:handleClients');
	}
	public function index()	{
		$clients = Client::withTrashed()->get();
		return view('clients.index', compact("clients"));
	}

	public function create()	{
		return view('clients.create');
	}

	public function store(Request $request)	{
		$data = $request->except('_token');
		$request->validate([
			'alias'		=> 'required|string|min:4|max:190|unique:clients',
			'reason'	=> 'required|string|max:190',
			'phone'		=> 'required|digits:10',
			'email'		=> 'nullable|email|max:190|unique:clients',
			'rfc'			=> 'nullable|alpha_num|size:13',
		]);
		$data['reason']	= mb_strtoupper($data['reason']);
		$data['rfc']		= mb_strtoupper($data['rfc']);
		$client = new Client;
		$client->fill($data);
		$client->save();
		$success = "Éxito al crear el cliente/proveedor <strong>$client->alias</strong> a las ".Date::now()->format('h:i:s a.');
		return redirect()->route('clients.index')->with('success', $success);
	}

	public function show($id)	{
		$client = Client::findOrFail($id);
		return view('clients.edit', compact('client'));
	}

	public function edit($id)	{
		return redirect()->route('clients.show',[$id]);
	}

	public function update(Request $request, $id)	{
		$data = $request->except(['_token','_method']);
		$client = Client::findOrFail($id);
		$request->validate([
			'alias'		=> 'required|string|min:4|max:190|unique:clients,alias,'.$client->id,
			'reason'	=> 'required|string|max:190',
			'phone'		=> 'required|digits:10',
			'email'		=> 'nullable|email|max:190|unique:clients,email,'.$client->id,
			'rfc'			=> 'nullable|alpha_num|size:13',
		]);
		$data['reason']	= mb_strtoupper($data['reason']);
		$data['rfc']		= mb_strtoupper($data['rfc']);
		$client->fill($data);
		$client->save();
		$success = "Éxito al actualizar el cliente/proveedor <strong>$client->alias</strong> a las ".Date::now()->format('h:i:s a.');
		return redirect()->route('clients.show',[$id])->with('success', $success);
	}

	public function destroy($id)	{
		$client = Client::withTrashed()->findOrFail($id);
		if($client->trashed()){
			$client->restore();
			return redirect()->route('clients.index')->with('info',"Éxito al <u>activar</u> el cliente/proveedor <strong>$client->alias</strong> a las ".Date::now()->format('h:i:s a.'));
		}else{
			$client->delete();
			return redirect()->route('clients.index')->with('info',"Éxito al <u>desactivar</u> el cliente/proveedor <strong>$client->alias</strong> a las ".Date::now()->format('h:i:s a.'));
		}
	}
}