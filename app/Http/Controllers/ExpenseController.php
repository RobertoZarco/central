<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expense;
use App\PaymentType;

class ExpenseController extends Controller	{
	public function __construct()	{
		$this->middleware('can:handleExpenses');
	}

	public function index()	{
		$expenses = Expense::all();
		return view('expenses.index', compact("expenses"));
	}

	public function create()	{
		$paymentTypes = PaymentType::all();
		return view('expenses.create', compact('paymentTypes'));
	}

	public function store(Request $request)	{
		$data = $request->except('_token');
		$this->validate($request,[
			'amount'			=> 'required|numeric',
			'description'	=> 'required|string',
			'payment_id'	=> 'required|integer|exists:payment_types,id'
		]);
		Expense::create($data);
		return redirect()->route('expenses.index')->with('success','Gasto creado satisfactoriamente');
	}

	public function show($id)	{
		return $this->edit($id);
	}

	public function edit($id)	{
		$expenses = Expense::find($id);
		$paymentTypes = PaymentType::all();
		return view('expenses.edit', compact('expenses', 'paymentTypes'));
	}

	public function update(Request $request, $id)	{
		$this->validate($request,[
			'amount'			=> 'required|numeric',
			'description'	=> 'required|string',
			'payment_id'	=> 'required|integer|exists:payment_types,id'
		]);
		Expense::find($id)->update($request->all());
		return redirect()->route('expenses.index')->with('success','Gasto actualizado satisfactoriamente');
	}

	public function destroy($id)	{
		Expense::find($id)->delete();
		return redirect()->route('expenses.index')->with('success','Gasto eliminado satisfactoriamente');
	}

}
