<?php

namespace App\Http\Controllers;

use App\User;
use App\Permits;
use Jenssegers\Date\Date;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller	{

	public function __construct()	{
		$this->middleware('can:handleUsers')->except(['editSelf','updateSelf']);
	}
	public function index()	{
		$users = User::withTrashed()->get()->except(auth()->user()->id);
		return view('users.index', compact('users'));
	}

	public function create()	{
		return view('users.create');
	}

	public function store(Request $request)	{
		$data = $request->except('_token');
		$request->validate([
			'username'	=> 'required|alpha_dash|min:4|max:190|unique:users',
			'name'			=> 'required|alpha|max:190',
			'paternal'	=> 'required|alpha|max:190',
			'maternal'	=> 'nullable|alpha|max:190',
			'phone'			=> 'nullable|digits:10',
			'mobile'		=> 'nullable|digits:10',
			'email'			=> 'nullable|email|max:190|unique:users',
			'password'	=> 'required|confirmed',
		]);
		$user = new User;
		$user->fill($data);
		$user->password = Hash::make($user->password);
		$user->save();
		$permits = new Permits;
		$permits->buys							= $request->get('buys') == 'on' ? 1 : 0;
		$permits->cashClosings			= $request->get('cashClosings') == 'on' ? 1 : 0;
		$permits->salesQuotations		= $request->get('salesQuotations') == 'on' ? 1 : 0;
		$permits->wastages					= $request->get('wastages') == 'on' ? 1 : 0;
		$permits->expenses					= $request->get('expenses') == 'on' ? 1 : 0;
		$permits->clientsProviders	= $request->get('clientsProviders') == 'on' ? 1 : 0;
		$permits->credits						= $request->get('credits') == 'on' ? 1 : 0;
		$permits->reports						= $request->get('reports') == 'on' ? 1 : 0;
		$permits->inventory					= $request->get('inventory') == 'on' ? 1 : 0;
		$permits->accounting				= $request->get('accounting') == 'on' ? 1 : 0;
		$permits->systemUsers				= $request->get('systemUsers') == 'on' ? 1 : 0;
		$permits->banks							= $request->get('banks') == 'on' ? 1 : 0;
		$permits->user()->associate($user);
		$permits->save();
		$success = "Éxito al crear el usuario <strong>$user->username</strong> a las ".Date::now()->format('h:i:s a.');
		return redirect()->route('users.index')->with('success', $success);
	}

	public function show($id)	{
		if($id == auth()->user()->id)
			return back();
		$user = User::findOrFail($id);
		return view('users.edit', compact('user'));
	}

	public function edit($id)	{
		if($id == auth()->user()->id)
			return back();
		return redirect()->route('users.show',[$id]);
	}

	public function update(Request $request, $id)	{
		$data = $request->except(['_token', '_method']);
		if(!$data['password'] || $data['password'] == '')
			unset($data['password']);
		$user = User::findOrFail($id);
		$request->validate([
			'username'	=> 'required|alpha_dash|min:4|max:190|unique:users,username,'.$user->id,
			'name'			=> 'required|string|max:190',
			'paternal'	=> 'required|alpha|max:190',
			'maternal'	=> 'nullable|alpha|max:190',
			'phone'			=> 'nullable|digits:10',
			'mobile'		=> 'nullable|digits:10',
			'email'			=> 'nullable|email|max:190|unique:users,email,'.$user->id,
			'password'	=> 'nullable|confirmed',
		]);
		$user->fill($data);
		if($request->get('password') && $request->get('password') != '')
			$user->password = Hash::make($user->password);
		$user->save();
		if($id != auth()->user()->id)	{
			$permits = Permits::firstOrCreate(['user_id' => $id]);
			$permits->buys							= $request->get('buys') == 'on' ? 1 : 0;
			$permits->cashClosings			= $request->get('cashClosings') == 'on' ? 1 : 0;
			$permits->salesQuotations		= $request->get('salesQuotations') == 'on' ? 1 : 0;
			$permits->wastages					= $request->get('wastages') == 'on' ? 1 : 0;
			$permits->expenses					= $request->get('expenses') == 'on' ? 1 : 0;
			$permits->clientsProviders	= $request->get('clientsProviders') == 'on' ? 1 : 0;
			$permits->credits						= $request->get('credits') == 'on' ? 1 : 0;
			$permits->reports						= $request->get('reports') == 'on' ? 1 : 0;
			$permits->inventory					= $request->get('inventory') == 'on' ? 1 : 0;
			$permits->accounting				= $request->get('accounting') == 'on' ? 1 : 0;
			$permits->systemUsers				= $request->get('systemUsers') == 'on' ? 1 : 0;
			$permits->banks							= $request->get('banks') == 'on' ? 1 : 0;
			$permits->save();
		}
		$success = "Éxito al actualizar el usuario <strong>$user->username</strong> a las ".Date::now()->format('h:i:s a.');
		if($id != auth()->user()->id)
			return redirect()->route('users.show',[$id])->with('success', $success);
		else
			return redirect()->route('users.editSelf')->with('success', $success);
	}

	public function destroy($id)	{
		if($id == auth()->user()->id)
			return back();
		$user = User::withTrashed()->findOrFail($id);
		if($user->trashed()){
			$user->restore();
			return redirect()->route('users.index')->with('info',"Éxito al <u>activar</u> el usuario <strong>$user->username</strong> a las ".Date::now()->format('h:i:s a.'));
		}else{
			$user->delete();
			return redirect()->route('users.index')->with('info',"Éxito al <u>desactivar</u> el usuario <strong>$user->username</strong> a las ".Date::now()->format('h:i:s a.'));
		}
	}

	public function editSelf(){
		$user = auth()->user();
		return view('users.edit', compact('user'));
	}

	public function updateSelf(Request $request){
		return $this->update($request, auth()->user()->id);
	}

}
