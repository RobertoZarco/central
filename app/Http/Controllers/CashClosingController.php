<?php

namespace App\Http\Controllers;

use App\Sale;
use App\PaymentType;
use Jenssegers\Date\Date;
use App\Exports\SalesExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;

class CashClosingController extends Controller	{

	public function __construct(){
		$this->middleware('can:handleCashClosings');
	}

	public function index()	{
		$salesToday = Sale::whereNull('expire_at')->whereDate('created_at', '=', Date::today()->toDateString())->get();
		$saleAmount = count($salesToday);
		$paymentTypes = PaymentType::all();
		$totalIncome = 0;
		foreach ($paymentTypes as $key => $paymentType) {
			$paymentTypes[$key]->amount = 0;
			$paymentTypes[$key]->total = 0;
			foreach ($salesToday as $sale)
				if($sale->payment_id == $paymentType->id){
					$paymentTypes[$key]->amount++;
					foreach ($sale->inventories as $itemSold) {
						$paymentTypes[$key]->total+=$itemSold->amount*$itemSold->salePrice;
						$totalIncome += $paymentTypes[$key]->total;
					}
				}
		}
		return view('cashClosings.index', compact('saleAmount', 'paymentTypes', 'totalIncome'));
	}

	public function generateReport(Excel $excel)	{
		$dateString = Date::now()->format('d_F_Y');
		return $excel->download(new SalesExport, "ventas_del_$dateString.xlsx");
	}

}
