<?php

namespace App\Http\Controllers;

use App\Credit;
use App\Client;
use App\User;
use Illuminate\Http\Request;

class CreditController extends Controller
{
	public function __construct(){
		$this->middleware('can:handleCredits');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $credits = Credit::selectRaw('id,paid,paid_at,client_id,user_id,created_at,updated_at,amount,
                                       CASE WHEN paid = "0" THEN "pendiente"
                                            WHEN paid = "1" THEN "pagado"
                                       ELSE "no existe el dato" 
                                       END AS pagado')
                                       ->get();

        return view('credits.index', compact("credits"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $clients   = Client::all();
         $users     = User::all();
         return view('credits.create',compact("clients","users"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       Credit::create($request->all());
      return redirect()->route('credits.index')->with('success','Credito agregado satisfactoriamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $credit = Credit::findOrFail($id);
        return view('credits.edit', compact('credit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect()->route('credits.show',[$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $montdecrement = $request->input('amount_payable');
        $credit = Credit::find($id);
        $credit->decrement('amount', $montdecrement);
        $credit->update();
        return redirect()->route('credits.index')->with('success','pago actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //dd($request);
         //Credit::find($id)->delete();
        //return redirect()->route('credits.index')->with('success','Credito eliminado satisfactoriamente');
    }
}
