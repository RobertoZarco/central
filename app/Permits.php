<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permits extends Model	{
	protected $primaryKey = 'user_id';
	protected $autoIncrement = false;

	public function user(){
		return $this->belongsTo('App\User');
	}
}
