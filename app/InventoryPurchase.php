<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryPurchase extends Model	{
	protected $fillable = [
		'amount', 'purchasePrice', 'inventory_id',
	];
	public function purchase(){
		return $this->belongsTo('App\Purchase');
	}
	public function inventory(){
		return $this->belongsTo('App\Inventory');
	}
	
}
