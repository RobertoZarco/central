<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model	{
	
	protected $fillable = ['amount','paid','paid_at','client_id','user_id'];

	public function user(){
		return $this->belongsTo('App\User');
	}
	public function client(){
		return $this->belongsTo('App\Client');
	}
}
