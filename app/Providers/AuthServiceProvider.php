<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider	{
	/**
	* The policy mappings for the application.
	*
	* @var array
	*/
	protected $policies = [
		'App\Model' => 'App\Policies\ModelPolicy',
	];

	/**
	* Register any authentication / authorization services.
	*
	* @return void
	*/
	public function boot()	{
		$this->registerPolicies();
		Gate::define('handleBuys', function ($user) {
			return $user->permits->buys == 1;
		});
		Gate::define('handleCashClosings', function ($user) {
			return $user->permits->cashClosings == 1;
		});
		Gate::define('handleSales', function ($user) {
			return $user->permits->salesQuotations == 1;
		});
		Gate::define('handleWastages', function ($user) {
			return $user->permits->wastages == 1;
		});
		Gate::define('handleExpenses', function ($user) {
			return $user->permits->expenses == 1;
		});
		Gate::define('handleClients', function ($user) {
			return $user->permits->clientsProviders == 1;
		});
		Gate::define('handleCredits', function ($user) {
			return $user->permits->credits == 1;
		});
		Gate::define('handleReports', function ($user) {
			return $user->permits->reports == 1;
		});
		Gate::define('handleInventory', function ($user) {
			return $user->permits->inventory == 1;
		});
		Gate::define('handleAccounting', function ($user) {
			return $user->permits->accounting == 1;
		});
		Gate::define('handleUsers', function ($user) {
			return $user->permits->systemUsers == 1;
		});
		Gate::define('handleBanks', function ($user) {
			return $user->permits->banks == 1;
		});
	}
}
