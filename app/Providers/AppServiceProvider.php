<?php

namespace App\Providers;

use Jenssegers\Date\Date;
use Illuminate\Support\Carbon;
use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider	{
	/**
	* Bootstrap any application services.
	*
	* @return void
	*/
	public function boot()	{
		Date::setLocale('es');
		Carbon::setLocale('es');
		Schema::defaultStringLength(190);
		Blade::directive('moneda', function ($money) {
			return "<?php echo '$ '.number_format($money, 2); ?>";
		});
	}

	/**
	* Register any application services.
	*
	* @return void
	*/
	public function register()	{
		if ($this->app->environment('local', 'testing')) {
			$this->app->register(DuskServiceProvider::class);
		}
	}
}
