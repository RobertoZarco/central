<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model	{
	
	public function provider(){
		return $this->belongsTo('App\Client', 'provider_id');
	}
	public function payment(){
		return $this->belongsTo('App\PaymentType');
	}
	public function inventories(){
		return $this->hasMany('App\InventoryPurchase');
	}

}
