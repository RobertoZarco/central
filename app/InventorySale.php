<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventorySale extends Model	{

	protected  $fillable = ['amount','salePrice','inventory_id','sale_id'];

	public function sale(){
		return $this->belongsTo('App\Sale');
	}
	public function inventory(){
		return $this->belongsTo('App\Inventory');
	}
}
