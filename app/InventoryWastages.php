<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryWastages extends Model
{


	protected  $fillable = ['amount','salePrice','lost','description','wastage_id','inventory_id'];

	public function wastages(){
		return $this->belongsTo('App\Wastage');
	}
	public function inventory(){
		return $this->belongsTo('App\Inventory');
	}


}
