<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wastage extends Model	{

	protected $fillable = ['description','user_id','sale_id'];

	public function client(){
		return $this->belongsTo('App\Client');
	}
	public function sale(){
		return $this->belongsTo('App\Sale');
	}

	public function InventoryWastages(){
		return $this->hasMany('App\InventoryWastages');
	}
	public function wastages(){
		return $this->hasMany('App\Wastage');
	}


}
