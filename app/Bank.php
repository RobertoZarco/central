<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model	{
	//use SoftDeletes;
	protected  $fillable = ['id','reason', 'rfc'];
	
	public function bills(){
		return $this->hasMany('App\Bill');
	}
}
