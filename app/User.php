<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable	{
	use Notifiable;
	use SoftDeletes;

	protected $fillable = [
		'username', 'name', 'paternal', 'maternal', 'phone', 'mobile', 'email', 'password',
	];
	protected $dates = [
		'deleted_at',
	];
	protected $hidden = [
		'password', 'remember_token',
	];

	public function getFullNameAttribute(){
		return ucwords(mb_strtolower("$this->name $this->paternal $this->maternal"));
	}

	public function permits(){
		return $this->hasOne('App\Permits');
	}
	public function bills(){
		return $this->hasMany('App\Bill');
	}
	public function credits(){
		return $this->hasMany('App\Credit');
	}
	public function wastages(){
		return $this->hasMany('App\Wastage');
	}
}
