<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model	{

	protected $fillable = ['description', 'amount', 'payment_id'];

	public function paymentType(){
		return $this->belongsTo('App\PaymentType', 'payment_id', 'id');
	}
}


