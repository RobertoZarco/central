<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model	{
	protected  $fillable = ['amount','discount'];

	public function client(){
		return $this->belongsTo('App\Client');
	}
	public function payment(){
		return $this->belongsTo('App\PaymentType');
	}
	public function inventories(){
		return $this->hasMany('App\InventorySale');
	}
	public function wastages(){
		return $this->hasMany('App\Wastage');
	}
}
