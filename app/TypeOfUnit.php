<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeOfUnit extends Model	{
	public function inventories(){
		return $this->hasMany('App\Inventory');
	}
}
