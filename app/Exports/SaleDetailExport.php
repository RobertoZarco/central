<?php

namespace App\Exports;

use App\Sale;
use App\InventorySale;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SaleDetailExport implements FromCollection, WithTitle, ShouldAutoSize, WithHeadings	{

	private $sale;
	private $counter;

	public function __construct(Sale $sale, int $counter)	{
		$this->sale			= $sale;
		$this->counter	= $counter;
	}

	public function collection()	{
		$data = [];
		$saleItems = InventorySale::where('sale_id',$this->sale->id)->orderBy('inventory_id')->get();
		foreach ($saleItems as $key => $saleItem) {
			$inventory = $saleItem->inventory;
			$data[] = [
				$key+1,
				ucfirst(mb_strtolower($inventory->unitType->key)),
				ucwords(mb_strtolower($inventory->name)),
				$saleItem->amount,
				ucfirst(mb_strtolower($inventory->category->key)),
				'$ '.number_format($saleItem->salePrice, 2),
			];
		}
		return collect($data);
	}

	public function title(): string	{
		return "Venta No. $this->counter";
	}

	public function headings(): array
	{
		return [
			'No.',
			'Unidad',
			'Nombre',
			'Cantidad',
			'Categoría',
			'Precio de venta',
			'     ',
			"Descuento: ".$this->sale->discount.'%',
			"Fecha y hora: ".$this->sale->created_at->toDateTimeString(),
			"Cliente: ".$this->sale->client->alias,
			"Tipo de pago: ".ucfirst(mb_strtolower($this->sale->payment->key)),
		];
	}

}
