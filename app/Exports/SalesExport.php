<?php

namespace App\Exports;

use App\Sale;
use Jenssegers\Date\Date;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class SalesExport implements WithMultipleSheets	{

	use Exportable;

	public function collection()	{
		return Sale::all();
	}

	public function sheets(): array	{
		$sheets = [];

		$sales = Sale::whereNull('expire_at')->whereDate('created_at', '=', Date::today()->toDateString())->get();

		foreach($sales as $key => $sale)
			$sheets[] = new SaleDetailExport($sale, $key+1);

		return $sheets;
	}

}
