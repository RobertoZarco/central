<?php

Route::get('/','Auth\LoginController@showLoginForm')->name('login');
Route::post('/login','Auth\LoginController@login');
Route::post('/logout','Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => 'auth'], function () {
	Route::get('/home','HomeController@index')->name('home');
	Route::get('/settings','UserController@editSelf')->name('users.editSelf');
	Route::patch('/settings','UserController@updateSelf')->name('users.updateSelf');
	Route::group(['middleware' => 'can:handleAccounting'], function () {

	});

	// MODULOS TERMINADOS O EN PROCESO
	Route::resources([
		//'sales'       => 'SaleController',
		'clients'     => 'ClientController',
		'users'       => 'UserController',
		'banks'       => 'BankController',
		'expenses'    => 'ExpenseController',
		'inventories' => 'InventoryController',
		'accounting'	=> 'AccountingController',	// 'shops'       => 'ShopController',
		//'wastages'    => 'WastageController',
		// 'reports'     => 'ReportController',
		'credits'     => 'CreditController'
	]);

	// Compras
	Route::get('/shops', 'ShopController@index')->name('shop');
	Route::post('/shops', 'ShopController@save')->name('shops.save');
	Route::patch('/shops', 'ShopController@filter')->name('shops.filter');
	Route::get('/shops/{purchase}', 'ShopController@ticket')->name('shop.ticket');
	Route::post('/shops/{inventory}', 'ShopController@addItem')->name('shops.addItem');
	// Corte de caja
	Route::get('/cashClosings', 'CashClosingController@index')->name('cashClosing');
	Route::get('/cashClosings/report', 'CashClosingController@generateReport')->name('cashClosing.report');
	// Reportes
	Route::get('/Reportindex','ReportController@showindex')->name('Reportindex');
	Route::get('/report/sales', 'ReportController@showformSales')->name('/report/sales');
	Route::get('/report/shops', 'ReportController@showFormShops')->name('/report/shops');
	Route::get('/report/expenses', 'ReportController@showFormExpenses')->name('/report/expenses');
	Route::get('/report/payments', 'ReportController@showFormPayments')->name('/report/payments');
	Route::post('/report/getQuerySales','ReportController@showQuerySales')->name('/report/getQuerySales');
	Route::post('/report/getQueryExpenses','ReportController@showQueryExpenses')->name('/report/getQueryExpenses');
	Route::post('/report/getQueryShops','ReportController@showQueryShops')->name('/report/getQueryShops');
	//ventas
	Route::get('/sales', 'SaleController@index')->name('sales.index');
	Route::get('/getInventory/{inventory}', 'InventoryController@getInventoryById')->name('inventory.getInventory');
	Route::post('/sales', 'SaleController@save')->name('sales.save');
	Route::get('/sales/{purchase}', 'SaleController@ticket');


	//devoluciones/merma
	Route::get('/wastages', 'WastageController@index')->name('wastages.getSales');
	Route::post('/wastages', 'WastageController@create')->name('wastages.wastageCreate');
	Route::get('/wastages/{purchase}', 'WastageController@ticket');


	//inventory_sales
	Route::get('/sale/{id}', 'SaleController@sale')->name('sales.saleDetails');

});
