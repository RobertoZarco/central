$(document).ready(function($) {

  let counter   = 1;
  let subTotal  = 0;
  let productos = [];
  let arreglo   = [];

  $("table.datatable").DataTable({
    processing:true,
    serverside:true,
    "autoWidth": false,
    order: [],
    scrollX: true,
    columnDefs: [
      {targets: "_all", "className": "text-left"},
      {targets: [-1, -2], "orderable": false},
    ],
    lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "Todos"]],
    language: {
      "lengthMenu"    : "Mostrar _MENU_ gastos por página",
      "zeroRecords"   : "No se encontraron gastos",
      "info"          : "Mostrando página _PAGE_ de _PAGES_",
      "infoEmpty"     : "No hay gastos disponibles",
      "search"        : "Buscar:",
      "infoFiltered"  : "(Filtrados de _MAX_ totales)",
      "loadingRecords": "Cargando...",
      "processing"    : "Procesando...",
      "paginate": {
        "first"   : "Primera",
        "last"    : "Última",
        "next"    : "Siguiente",
        "previous": "Anterior"
      },
    },
    initComplete: function(settings, json){
      $('select[name="DataTables_Table_0_length"]').select2({
        theme: "bootstrap",
        language: "es-MX",
      });
    },

    drawCallback: function(){
      actions();
    },
  });

  function actions(){
    let tableSale = $("#sale").DataTable();

    $('button[id^="btnAddSale"]').unbind('click').click(function(event) {
      let dataId     = $(this).attr('data-id');
      let dataAmount = $(this).attr('data-amount');
      let dataprice  = $(this).attr('data-price');
      let subTotal   = 0;

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
        url: "/getInventory/"+dataId,
        type: 'GET',
        dataType: 'json',
        // data: {"idProducto": dataId},
        beforeSend: function(){
          //alert(dataId);
        },
        success:function(data){
          //console.clear();

          let id           = data.id;
          let name         = data.name;
          let amount       = data.amount;
          let precioCompra = data.purchasePrice;
          let precioVenta  = data.salePrice;

          productos[id] = amount;

          //console.log(productos);

          tableSale.row.add([
            counter++,
            name,
            "<input type='text' name='cantidades' id='cantidades"+id+"' data-Id="+id+" data-amount="+amount+" data-precioVenta="+precioVenta+" class='cantidades' value='"+amount+"' size='3' >"+
            "<button type='button' class='btn btn-success' id='mas' data-Id="+id+" data-amount="+amount+" data-precioVenta="+precioVenta+" >"+
              "<span class='glyphicon glyphicon-plus' aria-hidden='true'></span>"+
            "</button>"+
            "<button type='button' class='btn btn-primary' id='menos' data-Id="+id+" data-amount="+amount+" data-precioVenta="+precioVenta+" >"+
              "<span class='glyphicon glyphicon-minus' aria-hidden='true'></span>"+
            "</button>",
            "<input type='text' id='price2' class='form-control' name='precioVenta' value='"+precioVenta+"' size='3' disabled>",
            "<input type='text' id='subTotal"+id+"' name='subTotal' class='form-control importe ' value='"+ amount * precioVenta +"' size='3' disabled>",
            "<span class='btn btn-danger glyphicon glyphicon-trash' dataId="+id+"></span>"
          ]).draw(false);

          tableSale.on('click', '.glyphicon-trash', function(){
            let row = tableSale.row($(this).parents('tr'));
            row.node().remove();
            row.remove();

            totales("cantidades", "amount");
            totales("importe", "subTotal");
            descuento();
          })


          $('input[class^="cantidades"]').unbind('keyup').keyup(function(event) {

            let cantidad    = $(this).attr('data-amount');
            let precioVenta = $(this).attr('data-precioVenta');
            let id          = $(this).attr('data-Id');

            //let valor = $("#cantidades"+id).val(parseInt($("#cantidades"+id).val()) + 1);
            let valor = $(this).val();

          	//console.log(valor+"|"+precioVenta);

            productos[id] = valor;

            //console.log(productos[id].val());

            let incremento = valor;

            subTotal = incremento * precioVenta;

            //console.log(subTotal);

            $("#subTotal"+id).val(subTotal);

            totales("cantidades", "amount");
            totales("importe", "subTotal");
            descuento();
          })

          $('button[id^="mas"]').unbind('click').click(function(event) {

            let cantidad    = $(this).attr('data-amount');
            let precioVenta = $(this).attr('data-precioVenta');
            let id          = $(this).attr('data-Id');
            //console.log( cantidad+"->"+precioVenta );
            let valor = $("#cantidades"+id).val(parseInt($("#cantidades"+id).val()) + 1);

            productos[id] = valor.val();

            //console.log(productos[id].val());

            let incremento = valor.val();

            subTotal = incremento * precioVenta;

            //console.log(subTotal);

            $("#subTotal"+id).val(subTotal);

            totales("cantidades", "amount");
            totales("importe", "subTotal");
            descuento();
          })

          $('button[id^="menos"]').unbind('click mouseup').click(function(event) {

            let cantidad    = $(this).attr('data-amount');
            let precioVenta = $(this).attr('data-precioVenta');
            let id          = $(this).attr('data-Id');
            //console.log( cantidad+"->"+precioVenta );
            let valor = $("#cantidades"+id).val(parseInt($("#cantidades"+id).val()) - 1);

            productos[id] = valor.val();
            console.log(productos[id]);

            let incremento = valor.val();

            subTotal = incremento * precioVenta;

            $("#subTotal"+id).val(subTotal);

            totales("cantidades", "amount");
            totales("importe", "subTotal");

            descuento();
          })

          totales("cantidades", "amount");
          totales("importe", "subTotal");
          descuento();

          $("#rebaja").bind('keyup mouseup', function(event) {
            let rebaja   = $(this).val();
            let subTotal = $("#subTotal").val();
            let descu    = subTotal * (rebaja / 100);
            let Total    = subTotal - descu;

            //console.log(rebaja);

            if(rebaja < -1){
              $("#descuento").val("0");
            }else{
              $("#descuento").val(descu);
            }

            $("#total").val(Total);

            totales("cantidades", "amount");
            totales("importe", "subTotal");
            descuento();
          });

          function descuento(){
            let rebaja   = $("#rebaja").val();
            let subTotal = $("#subTotal").val();
            let descu    = subTotal * (rebaja / 100);
            let Total    = subTotal - descu;

            //console.log(rebaja+"->"+subTotal+"->"+descu+"->"+Total);
            $("#descuento").val(descu);
            $("#total").val(Total);
          }
          //fin del success
        }
      })
    });
  }

  function totales(clase, elementId ){

    let cantidad_total = 0;

    $("."+clase).each(function(index, value) {
      //console.log("index->"+index+"----> valor ->"+value);
      if($.isNumeric($(this).val() )){
        cantidad_total = cantidad_total + eval($(this).val());
      }
    });

    $("#"+elementId).val(cantidad_total);
  }

  $("#venta").click(function(e){
    $("#vencimiento").slideUp('slow');
    $("#lblVencimiento").slideUp('slow');
    $("#vencimiento").val('');
    $("#venta").val(1);
    $("#cotizacion").val(0);
  });

  $("#cotizacion").click(function(e){
    $("#vencimiento").slideDown('slow');
    $("#lblVencimiento").slideDown('slow');
    $("#venta").val(0);
    $("#cotizacion").val(1);
  })

  $("#saveSale").click(function(e) {
    let amount      = $("#amount").val();
    let subTotal    = $("#subTotal").val();
    let rebaja      = $("#rebaja").val();
    let descuento   = $("#descuento").val();
    let total       = $("#total").val();
    let client_id   = $("#client").val();
    let payment_id  = $("#tipoPago").val();
    let venta       = $("#venta").val();
    let cotizacion  = $("#cotizacion").val();
    let vencimiento = $("#vencimiento").val();

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });


    $.ajax({
      url: '/sales',
      type: 'POST',
      dataType: 'html',
      data: { productos: productos,
              amount:amount,
              subTotal:subTotal,
              rebaja:rebaja,
              descuento:descuento,
              total:total,
              client_id:client_id,
              payment_id:payment_id,
              venta:venta,
              cotizacion:cotizacion,
              vencimiento:vencimiento
            },
      beforeSend:function(){
        $("#shopLoader").parent().show();
      },
      success:function(data){
        var response = jQuery.parseJSON(data);
        //console.log(response.sale_id);

        setTimeout(function(){
          var win = window.open('/sales/'+response.sale_id, '_blank');
          win.focus();
        }, 1500);

        $('#modalVenta').modal('show');

        $('.message').html(response.message);


        $("#accept").click(function(event) {
          location.reload(true);
        });

      }
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  });

});