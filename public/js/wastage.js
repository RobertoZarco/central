$(document).ready(function($) {

  $("#sales").DataTable({
    processing:true,
    serverside:true,
    "autoWidth": false,
    order: [],
    scrollX: true,
    columnDefs: [
      {targets: "_all", "className": "text-left"},
      {targets: [-1, -2], "orderable": false},
    ],
    lengthMenu: [[15, 25, 50, -1], [15, 25, 50, "Todos"]],
    language: {
      "lengthMenu"    : "Mostrar _MENU_ gastos por página",
      "zeroRecords"   : "No se encontraron gastos",
      "info"          : "Mostrando página _PAGE_ de _PAGES_",
      "infoEmpty"     : "No hay gastos disponibles",
      "search"        : "Buscar:",
      "infoFiltered"  : "(Filtrados de _MAX_ totales)",
      "loadingRecords": "Cargando...",
      "processing"    : "Procesando...",
      "paginate": {
        "first"   : "Primera",
        "last"    : "Última",
        "next"    : "Siguiente",
        "previous": "Anterior"
      },
    },
    initComplete: function(settings, json){
      $('select[name="DataTables_Table_0_length"]').select2({
        theme: "bootstrap",
        language: "es-MX",
      });
    },

    drawCallback: function(){
      //actions();
    },
  });

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  let tableSale = $("#sale").DataTable();
  let productos = [];
  let devo      = [];
  let merm      = [];
  let counter   = 1;

  $('button[id^="btnWastages"]').unbind('click').click(function(event) {

    let dataId     = $(this).attr('data-id');
    let dataAmount = $(this).attr('data-amount');
    let dataprice  = $(this).attr('data-price');
    let subTotal   = 0;

    $.ajax({
      url: "/getInventory/"+dataId,
      type: 'GET',
      dataType: 'json',
      // data: {"idProducto": dataId},
      beforeSend: function(){
        //alert(dataId);
      },
      success:function(data){
        //console.log(data);
        let id           = data.id;
        let name         = data.name;
        let amount       = data.amount;
        let precioCompra = data.purchasePrice;
        let precioVenta  = data.salePrice;

        tableSale.row.add([
          counter++,
          name,
          "<input type='text' name='cantidades' id='cantidades"+id+"' data-Id="+id+" class='cantidades' value='"+dataAmount+"' size='3' >"+
          "<button type='button' class='btn btn-success' id='mas' data-Id="+id+" data-amount="+dataAmount+" data-precioVenta="+dataprice+" >"+
            "<span class='glyphicon glyphicon-plus' aria-hidden='true'></span>"+
          "</button>"+
          "<button type='button' class='btn btn-primary' id='menos' data-Id="+id+" data-amount="+dataAmount+" data-precioVenta="+dataprice+" >"+
            "<span class='glyphicon glyphicon-minus' aria-hidden='true'></span>"+
          "</button>",
          "<input type='text' id='price2' class='form-control' name='precioVenta' value='"+dataprice+"' size='3' disabled>",
          "<input type='text' id='subTotal"+id+"' name='subTotal' class='form-control importe ' value='"+ dataAmount * dataprice +"' size='3' disabled>",
          "<div class='form-check'><label><input type='radio' name='wastages"+id+"' id='dev"+id+"' class='myRadio' value='1' checked><span class='label-text'>Devolución</span></label></div>",
          "<div class='form-check'><label><input type='radio' name='wastages"+id+"' id='merma"+id+"' class='myRadio' value='0'><span class='label-text'>Merma</span></label></div>",
          "<span class='btn btn-danger glyphicon glyphicon-trash' dataId="+id+"></span>"
        ]).draw(false);

        productos[data.id] = $("#cantidades"+id).val();

        tableSale.on('click', '.glyphicon-trash', function(){
          let row = tableSale.row($(this).parents('tr'));
          row.node().remove();
          row.remove();

          totales("cantidades", "amount");
          totales("importe", "subTotal");
          descuento();
        })

        $('button[id^="mas"]').unbind('click').click(function(event) {

          let cantidad    = $(this).attr('data-amount');
          let precioVenta = $(this).attr('data-precioVenta');
          let id          = $(this).attr('data-Id');
          let valor       = $("#cantidades"+id).val(parseInt($("#cantidades"+id).val()) + 1);

          productos[id] = valor.val();
          //devolucion[id] =

          let incremento = valor.val();
          subTotal = incremento * precioVenta;

          $("#subTotal"+id).val(subTotal);

          totales("cantidades", "amount");
          totales("importe", "subTotal");
          descuento();
        })

        $('button[id^="menos"]').unbind('click').click(function(event) {

          let cantidad    = $(this).attr('data-amount');
          let precioVenta = $(this).attr('data-precioVenta');
          let id          = $(this).attr('data-Id');
          //console.log( cantidad+"->"+precioVenta );
          let valor = $("#cantidades"+id).val(parseInt($("#cantidades"+id).val()) - 1);

          productos[id] = valor.val();
          //console.log(productos[id]);

          let incremento = valor.val();

          subTotal = incremento * precioVenta;

          $("#subTotal"+id).val(subTotal);

          totales("cantidades", "amount");
          totales("importe", "subTotal");

          descuento();
        })

        devo[id] = $("#dev"+id).val();
        merm[id] = $("#merma"+id).val();


        $("#dev"+id).click(function(event) {
          let valor = $(this).val(1);
          $("#merma"+id).val(0);
          devo[id] = $("#dev"+id).val();
          merm[id] = $("#merma"+id).val();
        });

        $("#merma"+id).click(function(event) {
          let valor = $(this).val(1);
          $("#dev"+id).val(0);
          devo[id] = $("#dev"+id).val();
          merm[id] = $("#merma"+id).val();
        });

        totales("cantidades", "amount");
        totales("importe", "subTotal");
        descuento();

        $("#rebaja").bind('keyup mouseup', function(event) {
          let rebaja   = $(this).val();
          let subTotal = $("#subTotal").val();
          let descu    = subTotal * (rebaja / 100);
          let Total    = subTotal - descu;

          //console.log(rebaja);

          if(rebaja < -1){
            $("#descuento").val("0");
          }else{
            $("#descuento").val(descu);
          }

          $("#total").val(Total);

          totales("cantidades", "amount");
          totales("importe", "subTotal");
          descuento();
        });

        function descuento(){
          let rebaja   = $("#rebaja").val();
          let subTotal = $("#subTotal").val();
          let descu    = subTotal * (rebaja / 100);
          let Total    = subTotal - descu;

          //console.log(rebaja+"->"+subTotal+"->"+descu+"->"+Total);
          $("#descuento").val(descu);
          $("#total").val(Total);
        }

        function totales(clase, elementId ){

          let cantidad_total = 0;

          $("."+clase).each(function(index, value) {
            //console.log("index->"+index+"----> valor ->"+value);
            if($.isNumeric($(this).val() )){
              cantidad_total = cantidad_total + eval($(this).val());
            }
          });

          $("#"+elementId).val(cantidad_total);
        }


        $("#saveWastages").unbind('click').click(function(e) {
          let saleId     = $("#saleId").val()
          let amount     = $("#amount").val();
          let subTotal   = $("#subTotal").val();
          let rebaja     = $("#rebaja").val();
          let descuento  = $("#descuento").val();
          let total      = $("#total").val();
          let client_id  = $("#clientId").val();
          let payment_id = $("#paymentTypeId").val();
          let devolucion = $("#dev").val();
          let merma      = $("#merma").val();
          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });

          console.log(devo);

          $.ajax({
            url: '/wastages',
            type: 'POST',
            dataType: 'html',
            data: { productos: productos,
                    wastages: devo,
                    amount:amount,
                    subTotal:subTotal,
                    rebaja:rebaja,
                    descuento:descuento,
                    total:total,
                    client_id:client_id,
                    payment_id:payment_id,
                    saleId:saleId

                  },
            beforeSend:function(){
              $("#shopLoader").parent().show();
            },
            success:function(data){

              var response = jQuery.parseJSON(data);
              //console.log(response.sale_id);

              $('#modalWastages').modal('show');

              $('.message').html(response.message)

              /*setTimeout(function(){
                var win = window.open('/wastage/'+response.wastage_id, '_blank');
                win.focus();
              }, 1500);*/

              $("#accept").click(function(event) {
                location.reload(true);
              });


            }
          })
          .done(function() {
            console.log("success");
          })
          .fail(function() {
            console.log("error");
          })
          .always(function() {
            console.log("complete");
          });

        });



        //fin del success
      }
    })

  });



});