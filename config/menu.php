<?php

use Spatie\Menu\Laravel\Menu;
use Spatie\Menu\Laravel\Html;
use Spatie\Menu\Laravel\Link;

Menu::macro('adminlteMenu', function () {
	return Menu::new()->addClass('sidebar-menu')->setAttribute('data-widget','tree');
});
Menu::macro('adminlteSubmenu', function ($text, $icon = 'fas fa-link', $color = 'white') {
	return Menu::new()->prepend("<a href='#'><i class='$icon text-$color'></i>&nbsp;&nbsp;<span>$text</span><i class='fa fa-angle-left pull-right'></i></a>")->addParentClass('treeview')->addClass('treeview-menu');
});
Menu::macro('adminlteSeparator', function ($title) {
	return Html::raw($title)->addParentClass('header');
});
Menu::macro('adminlteDefaultMenu', function ($text, $icon = 'fas fa-link', $color = 'white') {
	return Html::raw("<i class='$icon text-$color'></i>&nbsp;&nbsp;<span>$text</span>")->html();
});

Menu::macro('sidebar', function () {
	return Menu::adminlteMenu()
	->add(Menu::adminlteSeparator('PÁGINAS'))
	->link('/home', Menu::adminlteDefaultMenu('Inicio', 'fas fa-home', 'purple'))
	->link('/settings', Menu::adminlteDefaultMenu('Mi cuenta', 'fas fa-user', 'purple'))
	->add(Menu::adminlteSeparator('MÓDULOS'))

	->linkIf(Auth::user()->can('handleBuys'),'/shops',
			Menu::adminlteDefaultMenu('Compras', 'fas fa-shopping-cart', 'blue')
		)

	->linkIf(Auth::user()->can('handleCashClosings'),'/cashClosings',
			Menu::adminlteDefaultMenu('Corte de caja', 'fas fa-calendar-check', 'blue')
		)

	->linkIf(Auth::user()->can('handleCashClosings'),'/sales',
			Menu::adminlteDefaultMenu('Venta/Cotizaciones', 'fas fa-handshake', 'blue')
		)

	/*
	->addIf(Auth::user()->can('handleSales'),
		Menu::adminlteSubmenu('Ventas/Cotizaciones', 'fas fa-handshake', 'blue')
			->add(Link::to('/sales', 'Ventas'))
			->add(Link::to('/sales_quotations', 'Detalle de ventas/cotizaciones'))
  	)
	*/

	->linkIf(Auth::user()->can('handleWastages'),'/wastages',
			Menu::adminlteDefaultMenu('Devoluciones/Merma', 'fas fa-thumbs-down', 'green')
		)

	->linkIf(Auth::user()->can('handleExpenses'),'/expenses',
			Menu::adminlteDefaultMenu('Gastos', 'fas fa-people-carry', 'green')
		)

	->linkIf(Auth::user()->can('handleClients'),'/clients',
			Menu::adminlteDefaultMenu('Clientes/Proveedores', 'fas fa-user-tie', 'green')
		)

	->linkIf(Auth::user()->can('handleCredits'),'/credits',
			Menu::adminlteDefaultMenu('Créditos', 'fas fa-hand-holding-usd', 'yellow')
		)

	->linkIf(Auth::user()->can('handleReports'),'/Reportindex',
			Menu::adminlteDefaultMenu('Reportes', 'fas fa-chart-line', 'yellow')
		)

	->linkIf(Auth::user()->can('handleInventory'),'/inventories',
			Menu::adminlteDefaultMenu('Inventario', 'fas fa-cubes', 'yellow')
		)

	
	->linkIf(Auth::user()->can('handleAccounting'),'/accounting',
			Menu::adminlteDefaultMenu('Contaduría', 'fas fa-business-time', 'red')
		)

	->linkIf(Auth::user()->can('handleUsers'),'/users',
			Menu::adminlteDefaultMenu('Usuarios', 'fas fa-users-cog', 'red')
		)

	/*->linkIf(Auth::user()->can('handleBanks'),'banks',
			Menu::adminlteDefaultMenu('Bancos', 'fas fa-money-check-alt', 'red')
		)*/
    ->add(Menu::adminlteSubmenu('Bancos','fas fa-archway','red')
	 	->addParentClass('treeview')
	 	->add(Link::to('/banks', 'Administrar Bancos'))
	 	/*->add(Link::to('/link2', 'Link2'))*/
	 )

	->setActiveFromRequest();
});
	// ->add(Menu::adminlteSubmenu('Submenu1')
	// 	->addParentClass('treeview')
	// 	->add(Link::to('/link1', 'Link1'))
	// 	->add(Link::to('/link2', 'Link2'))
	// 	->url('http://www.google.com', 'Google')
	// )