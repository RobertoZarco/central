<?php

use App\TypeOfUnit;
use Illuminate\Database\Seeder;

class TypesUnitsSeeder extends Seeder	{
	public function run()	{
		TypeOfUnit::insert([
			[
				'key' => 'PIEZA',
				'description' => 'El producto se mide en piezas',
				'created_at' => Date::now(),
				'updated_at' => Date::now(),
			],[
				'key' => 'KILO',
				'description' => 'El producto se mide en kilogramos',
				'created_at' => Date::now(),
				'updated_at' => Date::now(),
			],[
				'key' => 'CAJA',
				'description' => 'El producto se mide en cajas',
				'created_at' => Date::now(),
				'updated_at' => Date::now(),
			],[
				'key' => 'MANOJO',
				'description' => 'El producto se mide en manojos',
				'created_at' => Date::now(),
				'updated_at' => Date::now(),
			],[
				'key' => 'COSTAL',
				'description' => 'El producto se mide en costales',
				'created_at' => Date::now(),
				'updated_at' => Date::now(),
			],[
				'key' => 'LITRO',
				'description' => 'El producto se mide en litros',
				'created_at' => Date::now(),
				'updated_at' => Date::now(),
			],
		]);
	}
}
