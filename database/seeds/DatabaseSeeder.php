<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder	{
	public function run()	{
		$this->call(UsersTableSeeder::class);
		$this->call(TypesUnitsSeeder::class);
		$this->call(PaymentTypesSeeder::class);
		$this->call(CategoriesSeeder::class);
		$this->call(ProductsSeeder::class);
	}
}
