<?php

use App\PaymentType;
use Illuminate\Database\Seeder;

class PaymentTypesSeeder extends Seeder	{
	public function run()	{
		PaymentType::insert([
			[
				'key' => 'EFECTIVO',
				'description' => 'Pago con dinero en efectivo',
				'created_at' => Date::now(),
				'updated_at' => Date::now(),
			],[
				'key' => 'CHEQUE',
				'description' => 'Pago con cheque a depositar a banco',
				'created_at' => Date::now(),
				'updated_at' => Date::now(),
			],[
				'key' => 'TRANSFERENCIA',
				'description' => 'Pago con transferencia bancaria',
				'created_at' => Date::now(),
				'updated_at' => Date::now(),
			],[
				'key' => 'TARJETA CRÉDITO',
				'description' => 'Pago con tarjeta de crédito',
				'created_at' => Date::now(),
				'updated_at' => Date::now(),
			],[
				'key' => 'TARJETA DÉBITO',
				'description' => 'Pago con tarjeta de débito',
				'created_at' => Date::now(),
				'updated_at' => Date::now(),
			],[
				'key' => 'A CRÉDITO',
				'description' => 'Pago con crédito, se registra un adeudo',
				'created_at' => Date::now(),
				'updated_at' => Date::now(),
			],
		]);
	}
}