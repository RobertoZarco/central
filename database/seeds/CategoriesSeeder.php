<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder	{
	public function run()	{
		Category::insert([
			[
				'key' => 'FRUTAS',
				'description' => 'El producto se clasifica dentro de frutas',
				'created_at' => Date::now(),
				'updated_at' => Date::now(),
			],[
				'key' => 'VERDURAS',
				'description' => 'El producto se clasifica dentro de verduras',
				'created_at' => Date::now(),
				'updated_at' => Date::now(),
			],[
				'key' => 'LEGUMBRES',
				'description' => 'El producto se clasifica dentro de legumbres',
				'created_at' => Date::now(),
				'updated_at' => Date::now(),
			],[
				'key' => 'LÁCTEOS',
				'description' => 'El producto se clasifica dentro de lácteos',
				'created_at' => Date::now(),
				'updated_at' => Date::now(),
			],[
				'key' => 'VARIOS',
				'description' => 'El producto se clasifica dentro de varios',
				'created_at' => Date::now(),
				'updated_at' => Date::now(),
			],
		]);
	}
}
