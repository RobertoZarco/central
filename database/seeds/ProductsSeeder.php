<?php

use App\Inventory;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder	{
	public function run()	{
		Inventory::insert([
			['category_id' => 1, 'name' => 'NARANJA EN ARPILLA 5 KG', 'purchasePrice' => 110.00, 'salePrice' => 110.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'NARANJA EN ARPILLA 9 KG', 'purchasePrice' => 98.00, 'salePrice' => 98.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'MELON VERDE', 'purchasePrice' => 9.00, 'salePrice' => 9.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'REPOLLO', 'purchasePrice' => 3.00, 'salePrice' => 3.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'HOJA DE LAUREL ROLLO', 'purchasePrice' => 30.00, 'salePrice' => 30.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'MANZANA GRANNY SMITH', 'purchasePrice' => 90.00, 'salePrice' => 90.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'MANZANA GRANNY SMITH', 'purchasePrice' => 600.00, 'salePrice' => 600.00, 'unit_id' => 3, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'BROCOLI Coronas', 'purchasePrice' => 12.00, 'salePrice' => 12.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'COL DE BRUSELAS', 'purchasePrice' => 35.00, 'salePrice' => 35.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'TORONJA', 'purchasePrice' => 6.30, 'salePrice' => 6.30, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'COLIRABANO MORADO', 'purchasePrice' => 43136.00, 'salePrice' => 43136.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'LECHUGA ESCAROLA', 'purchasePrice' => 43285.00, 'salePrice' => 43285.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'DURAZNO', 'purchasePrice' => 43218.00, 'salePrice' => 43218.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'DURAZNO', 'purchasePrice' => 43219.00, 'salePrice' => 43219.00, 'unit_id' => 3, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CALABAZA DE CASTILLA', 'purchasePrice' => 43228.00, 'salePrice' => 43228.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'BROCOLI Baby', 'purchasePrice' => 12.00, 'salePrice' => 12.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'KIWI REGULAR', 'purchasePrice' => 42.00, 'salePrice' => 42.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CEBOLLA BLANCA', 'purchasePrice' => 43285.00, 'salePrice' => 43285.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'COLIFLOR', 'purchasePrice' => 43229.00, 'salePrice' => 43229.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'GERMEN DE APIO', 'purchasePrice' => 39.20, 'salePrice' => 39.20, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'PORO', 'purchasePrice' => 43147.00, 'salePrice' => 43147.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'ZANAHORIA LEÑA', 'purchasePrice' => 100.00, 'salePrice' => 100.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'ZANAHORIA LEÑA', 'purchasePrice' => 100.00, 'salePrice' => 100.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'EPAZOTE', 'purchasePrice' => 43222.00, 'salePrice' => 43222.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'LIMON PERSA', 'purchasePrice' => 5.00, 'salePrice' => 5.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'UVA BLANCA S/SEMILLA', 'purchasePrice' => 130.00, 'salePrice' => 130.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'UVA BLANCA S/SEMILLA', 'purchasePrice' => 130.00, 'salePrice' => 130.00, 'unit_id' => 3, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'AGUACATE HAAS', 'purchasePrice' => 35.00, 'salePrice' => 35.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'MANGO ATAULFO', 'purchasePrice' => 5.00, 'salePrice' => 5.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'PEPINO', 'purchasePrice' => 43226.00, 'salePrice' => 43226.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'RABANO CAMBRAY ROLLO', 'purchasePrice' => 6.00, 'salePrice' => 6.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CHICHARO', 'purchasePrice' => 15.00, 'salePrice' => 15.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'MANZANA RED DELICIOUS', 'purchasePrice' => 48.00, 'salePrice' => 48.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'MANZANA RED DELICIOUS', 'purchasePrice' => 550.00, 'salePrice' => 550.00, 'unit_id' => 3, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'MANZANA ROYAL GALA', 'purchasePrice' => 43.50, 'salePrice' => 43.50, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'MANZANA ROYAL GALA', 'purchasePrice' => 480.00, 'salePrice' => 480.00, 'unit_id' => 3, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'AGUACATE HAAS', 'purchasePrice' => 35.00, 'salePrice' => 35.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'PLATANO MACHO', 'purchasePrice' => 43112.00, 'salePrice' => 43112.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'FRESA DOMO Strawberries', 'purchasePrice' => 30.00, 'salePrice' => 30.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'GUANABANA', 'purchasePrice' => 40.00, 'salePrice' => 40.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'NOPAL CON ESPINA', 'purchasePrice' => 43228.00, 'salePrice' => 43228.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'COCO CON CASCARA', 'purchasePrice' => 7.00, 'salePrice' => 7.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'COCO SIN CASCARA', 'purchasePrice' => 7.00, 'salePrice' => 7.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'GUAYABA', 'purchasePrice' => 11.00, 'salePrice' => 11.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'MELON CANTALUPE', 'purchasePrice' => 43234.00, 'salePrice' => 43234.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'NARANJA DULCE', 'purchasePrice' => 43108.00, 'salePrice' => 43108.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'GERMEN DE SOYA', 'purchasePrice' => 39.20, 'salePrice' => 39.20, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'COLIFLOR VERDE', 'purchasePrice' => 18.00, 'salePrice' => 18.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'COLIFLOR MORADO', 'purchasePrice' => 18.00, 'salePrice' => 18.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'MANGO ATAULFO', 'purchasePrice' => 43144.00, 'salePrice' => 43144.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'ACELGA', 'purchasePrice' => 43223.00, 'salePrice' => 43223.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'ACELGA', 'purchasePrice' => 3.50, 'salePrice' => 3.50, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'JENGIBRE', 'purchasePrice' => 43154.00, 'salePrice' => 43154.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'JICAMA', 'purchasePrice' => 43229.00, 'salePrice' => 43229.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'UVA GLOBO IMPORTADA', 'purchasePrice' => 40.00, 'salePrice' => 40.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'UVA GLOBO IMPORTADA', 'purchasePrice' => 40.00, 'salePrice' => 40.00, 'unit_id' => 3, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'UVA ROJA SIN SEMILLA', 'purchasePrice' => 34.70, 'salePrice' => 34.70, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'UVA ROJA SIN SEMILLA', 'purchasePrice' => 34.70, 'salePrice' => 34.70, 'unit_id' => 3, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'EJOTE', 'purchasePrice' => 43326.00, 'salePrice' => 43326.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'ELOTE', 'purchasePrice' => 43134.00, 'salePrice' => 43134.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'ALBAHACA', 'purchasePrice' => 3.50, 'salePrice' => 3.50, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'ALBAHACA', 'purchasePrice' => 3.50, 'salePrice' => 3.50, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'ALBAHACA', 'purchasePrice' => 3.50, 'salePrice' => 3.50, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'CILANTRO', 'purchasePrice' => 3.00, 'salePrice' => 3.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'PEREJIL', 'purchasePrice' => 43223.00, 'salePrice' => 43223.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CALABAZA HALLOWEEN', 'purchasePrice' => 11.00, 'salePrice' => 11.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CALABAZA BRUJITA', 'purchasePrice' => 11.00, 'salePrice' => 11.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CALABAZA ITALIANA', 'purchasePrice' => 245.00, 'salePrice' => 245.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CEBOLLA ROJA', 'purchasePrice' => 43294.00, 'salePrice' => 43294.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CHAMPIÑON', 'purchasePrice' => 52.20, 'salePrice' => 52.20, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CHAYOTE', 'purchasePrice' => 43256.00, 'salePrice' => 43256.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CHILE ANCHO', 'purchasePrice' => 63.00, 'salePrice' => 63.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CHILE CASCABEL', 'purchasePrice' => 110.00, 'salePrice' => 110.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CHILE CHIPOTLE', 'purchasePrice' => 73.00, 'salePrice' => 73.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CHILE DE ARBOL', 'purchasePrice' => 53.00, 'salePrice' => 53.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CHILE GUAJILLO', 'purchasePrice' => 50.00, 'salePrice' => 50.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CHILE HABANERO', 'purchasePrice' => 75.00, 'salePrice' => 75.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CHILE GÜERO', 'purchasePrice' => 23.00, 'salePrice' => 23.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CHILE JALAPEÑO', 'purchasePrice' => 43297.00, 'salePrice' => 43297.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CHILE PASILLA', 'purchasePrice' => 73.00, 'salePrice' => 73.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CHILE PIMIENTO AMARILLO', 'purchasePrice' => 15.00, 'salePrice' => 15.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CHILE PIMIENTO NARANJA', 'purchasePrice' => 35.00, 'salePrice' => 35.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CHILE PIMIENTO ROJO', 'purchasePrice' => 15.00, 'salePrice' => 15.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CHILE PIMIENTO VERDE', 'purchasePrice' => 15.00, 'salePrice' => 15.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CHILE POBLANO', 'purchasePrice' => 43300.00, 'salePrice' => 43300.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CHILE SERRANO', 'purchasePrice' => 43153.00, 'salePrice' => 43153.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'CILANTRO ROLLO', 'purchasePrice' => 30.00, 'salePrice' => 30.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'CIRUELA', 'purchasePrice' => 43249.00, 'salePrice' => 43249.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'NARANJA EN ARPILLA 10 KG', 'purchasePrice' => 130.00, 'salePrice' => 130.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'NARANJA EN ARPILLA 14 KG', 'purchasePrice' => 70.00, 'salePrice' => 70.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'NARANJA EN ARPILLA 18 KG', 'purchasePrice' => 39.00, 'salePrice' => 39.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'NARANJA EN ARPILLA 20 KG', 'purchasePrice' => 63.00, 'salePrice' => 63.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'NARANJA PARA JUGO', 'purchasePrice' => 43350.00, 'salePrice' => 43350.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'PAPA BLANCA', 'purchasePrice' => 43353.00, 'salePrice' => 43353.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'PAPA JUMBO', 'purchasePrice' => 43299.00, 'salePrice' => 43299.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'PAPA RIPIO', 'purchasePrice' => 43143.00, 'salePrice' => 43143.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'PAPAYA MARADOL', 'purchasePrice' => 43234.00, 'salePrice' => 43234.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'PERA DE ANJU', 'purchasePrice' => 34.00, 'salePrice' => 34.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'PERA DE ANJU', 'purchasePrice' => 480.00, 'salePrice' => 480.00, 'unit_id' => 3, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'PEREJIL ROLLO', 'purchasePrice' => 40.00, 'salePrice' => 40.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'PEREJIL CHINO', 'purchasePrice' => 16.00, 'salePrice' => 16.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'PEREJIL CHINO ROLLO', 'purchasePrice' => 120.00, 'salePrice' => 120.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'PIÑA MIEL', 'purchasePrice' => 10.00, 'salePrice' => 10.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'PORO ROLLO', 'purchasePrice' => 60.00, 'salePrice' => 60.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'RABANO CAMBRAY EN BOLSA', 'purchasePrice' => 6.00, 'salePrice' => 6.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'RABANO LARGO', 'purchasePrice' => 43227.00, 'salePrice' => 43227.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'REPOLLO BOLA', 'purchasePrice' => 43232.00, 'salePrice' => 43232.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'REPOLLO BULTO', 'purchasePrice' => 200.00, 'salePrice' => 200.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'ROMERITO', 'purchasePrice' => 14.00, 'salePrice' => 14.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'SANDIA', 'purchasePrice' => 43226.00, 'salePrice' => 43226.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'SETA', 'purchasePrice' => 55.00, 'salePrice' => 55.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'TE DE MANZANILLA', 'purchasePrice' => 43103.00, 'salePrice' => 43103.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'TE DE MANZANILLA ROLLO', 'purchasePrice' => 20.00, 'salePrice' => 20.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'TEJOCOTE', 'purchasePrice' => 43234.00, 'salePrice' => 43234.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'TOMATE BOLA', 'purchasePrice' => 43234.00, 'salePrice' => 43234.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'TOMATE BOLA', 'purchasePrice' => 300.00, 'salePrice' => 300.00, 'unit_id' => 3, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'TOMATE SALADET', 'purchasePrice' => 43234.00, 'salePrice' => 43234.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'TOMATE SALADET', 'purchasePrice' => 450.00, 'salePrice' => 450.00, 'unit_id' => 3, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'TOMATE VERDE', 'purchasePrice' => 43148.00, 'salePrice' => 43148.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'TOMATE VERDE PELADO', 'purchasePrice' => 43233.00, 'salePrice' => 43233.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 3, 'name' => 'TOMILLO', 'purchasePrice' => 3.00, 'salePrice' => 3.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'TUNA', 'purchasePrice' => 11.00, 'salePrice' => 11.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'ZANAHORIA MEDIANA', 'purchasePrice' => 43347.00, 'salePrice' => 43347.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'ZANAHORIA MEDIANA', 'purchasePrice' => 220.00, 'salePrice' => 220.00, 'unit_id' => 1, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'BLUE BERRY', 'purchasePrice' => 55.00, 'salePrice' => 55.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 1, 'name' => 'ZARZAMORA', 'purchasePrice' => 40.00, 'salePrice' => 40.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
			['category_id' => 2, 'name' => 'CEBOLLA CAMBRAY', 'purchasePrice' => 43285.00, 'salePrice' => 43285.00, 'unit_id' => 2, 'created_at' => Date::now(), 'updated_at' => Date::now() ],
		]);
	}
}
