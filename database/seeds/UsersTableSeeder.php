<?php

use App\User;
use App\Permits;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder	{
	public function run()	{
		$user = new User;
		$user->username	= 'admin';
		$user->name			= 'admin';
		$user->email		= 'admin@example.com';
		$user->paternal	= 'administrador';
		$user->password	= Hash::make('123456');
		$user->save();
		$permits = new Permits;
		$permits->buys							= 1;
		$permits->cashClosings			= 1;
		$permits->salesQuotations		= 1;
		$permits->wastages					= 1;
		$permits->expenses					= 1;
		$permits->clientsProviders	= 1;
		$permits->credits						= 1;
		$permits->reports						= 1;
		$permits->inventory					= 1;
		$permits->accounting				= 1;
		$permits->systemUsers				= 1;
		$permits->banks							= 1;
		$permits->user()->associate($user);
		$permits->save();
	}
}
