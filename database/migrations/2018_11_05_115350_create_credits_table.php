<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditsTable extends Migration	{
	public function up()	{
		Schema::create('credits', function (Blueprint $table) {
			$table->increments('id');
			$table->double('amount')->unsigned();
			$table->boolean('paid')->default(0);
			$table->timestamp('paid_at')->nullable();
			$table->integer('client_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->timestamps();
			$table->integer('sale_id')->unsigned();

			$table->index(['id', 'client_id', 'user_id', 'sale_id']);

			$table->foreign('client_id')->references('id')->on('clients')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('sale_id')->references('id')->on('sales')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	public function down()	{
		Schema::dropIfExists('credits');
	}
}
