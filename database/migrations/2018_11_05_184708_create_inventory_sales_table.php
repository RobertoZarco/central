<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventorySalesTable extends Migration	{

	public function up()	{
		Schema::create('inventory_sales', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('amount');
			$table->double('salePrice', 25, 2)->unsigned();
			$table->integer('inventory_id')->unsigned();
			$table->integer('sale_id')->unsigned();
			$table->timestamps();

			$table->index(['id', 'inventory_id', 'sale_id']);

			$table->foreign('inventory_id')->references('id')->on('inventories')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('sale_id')->references('id')->on('sales')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	public function down()	{
		Schema::dropIfExists('inventory_sales');
	}
	
}
