<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryWastagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_wastages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('amount')->unsigned();
            $table->double('salePrice', 25, 2)->unsigned();
            $table->boolean('lost')->default(0);
            $table->text('description')->nullable();
            $table->integer('wastage_id')->unsigned();
            $table->integer('inventory_id')->unsigned();
            $table->timestamps();

            $table->index(['id', 'wastage_id', 'inventory_id']);

            $table->foreign('wastage_id')->references('id')->on('wastages')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('inventory_id')->references('id')->on('inventories')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_wastages');
    }
}
