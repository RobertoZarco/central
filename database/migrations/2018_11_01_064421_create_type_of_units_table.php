<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeOfUnitsTable extends Migration	{

	public function up()	{
		Schema::create('type_of_units', function (Blueprint $table) {
			$table->increments('id');
			$table->string('key');
			$table->string('description');
			$table->timestamps();

			$table->index(['id', 'key']);
		});
	}

	public function down()	{
		Schema::dropIfExists('type_of_units');
	}

}
