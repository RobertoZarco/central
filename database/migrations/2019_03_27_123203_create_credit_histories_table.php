<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->double('initial_amount')->nullable(false);
            $table->double('previous_amount')->nullable(false);
            $table->double('amount_paid')->nullable(false);
            $table->string('status',1)->nullable()->comment('P = pagado', 'D = deudor');
            $table->integer('credit_id')->unsigned();

            $table->timestamps();

            $table->index(['id','credit_id']);

            //$table->foreign('client_id')->references('id')->on('clients')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('credit_id')->references('id')->on('credits')->onDelete('restrict')->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_histories');
    }
}

/*
            $table->integer('client_id')->nullable(false);
            $table->integer('credit_id')->nullable(false);

            $table->double('initial_amount')->nullable(false);
            $table->double('previous_amount')->nullable(false);
            $table->double('amount paid')->nullable(false);
            $table->string('status',1)->nullable()->comment('P = pagado', 'D = deudor');
            $table->timestamps();

            $table->index(['id', 'client_id', 'credit_id']);
            */