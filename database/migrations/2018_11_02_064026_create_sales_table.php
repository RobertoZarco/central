<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration	{

	public function up()	{
		Schema::create('sales', function (Blueprint $table) {
			$table->increments('id');
			$table->tinyInteger('discount')->unsigned()->default(0);
			$table->timestamp('expire_at')->nullable();
			$table->integer('client_id')->unsigned();
			$table->integer('payment_id')->unsigned();
			$table->timestamps();

			$table->index(['id', 'client_id', 'payment_id']);


			$table->foreign('client_id')->references('id')->on('clients')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('payment_id')->references('id')->on('payment_types')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	public function down()	{
		Schema::dropIfExists('sales');
	}

}
