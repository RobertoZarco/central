<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration	{

	public function up()	{
		Schema::create('bills', function (Blueprint $table) {
			$table->increments('id');
			$table->double('amount')->unsigned();
			$table->boolean('entrance')->default(0);
			$table->integer('client_id')->unsigned();
			$table->integer('bank_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->timestamps();


			$table->index(['bank_id', 'client_id', 'user_id']);

			$table->foreign('client_id')->references('id')->on('clients')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('bank_id')->references('id')->on('banks')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

		});
	}

	public function down()	{
		Schema::dropIfExists('bills');
	}
}
