<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoriesTable extends Migration	{

	public function up()	{
		Schema::create('inventories', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->integer('amount')->default(0);
			$table->double('purchasePrice', 15, 2)->unsigned();
			$table->double('salePrice', 15, 2)->unsigned();
			$table->integer('unit_id')->unsigned();
			$table->integer('category_id')->unsigned();
			$table->timestamps();
			$table->softDeletes();

			$table->index(['id', 'unit_id', 'category_id']);

			$table->foreign('unit_id')->references('id')->on('type_of_units')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('category_id')->references('id')->on('categories')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	public function down()	{
		Schema::dropIfExists('inventories');
	}

}