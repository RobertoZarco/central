<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration	{

	public function up()	{
		Schema::create('purchases', function (Blueprint $table) {
			$table->increments('id');
			$table->tinyInteger('discount')->unsigned()->default(0);
			$table->integer('provider_id')->unsigned();
			$table->integer('payment_id')->unsigned();
			$table->timestamps();

			$table->index(['id', 'provider_id', 'payment_id']);

			$table->foreign('provider_id')->references('id')->on('clients')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('payment_id')->references('id')->on('payment_types')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	public function down()	{
		Schema::dropIfExists('purchases');
	}

}
