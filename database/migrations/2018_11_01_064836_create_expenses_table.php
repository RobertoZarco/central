<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration	{

	public function up()	{
		Schema::create('expenses', function (Blueprint $table) {
			$table->increments('id');
			$table->double('amount')->unsigned();
			$table->text('description');
			$table->integer('payment_id')->unsigned();
			$table->timestamps();

			$table->index(['id', 'payment_id']);

			$table->foreign('payment_id')->references('id')->on('payment_types')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	public function down()	{
		Schema::dropIfExists('expenses');
	}
}
