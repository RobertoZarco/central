<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration	{

	public function up()	{
		Schema::create('users', function (Blueprint $table) {
			$table->increments('id');
			$table->string('username')->unique()->nullable();
			$table->string('name');
			$table->string('paternal');
			$table->string('maternal')->nullable();
			$table->string('phone',10)->nullable();
			$table->string('mobile',10)->nullable();
			$table->string('email')->nullable()->unique();
			$table->string('password');
			$table->rememberToken();
			$table->timestamps();
			$table->softDeletes();

			$table->index(['id', 'username']);
		});
	}

	public function down()	{
		Schema::dropIfExists('users');
	}

}
