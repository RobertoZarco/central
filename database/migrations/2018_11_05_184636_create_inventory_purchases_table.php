<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryPurchasesTable extends Migration	{

	public function up()	{
		Schema::create('inventory_purchases', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('amount')->unsigned();
			$table->double('purchasePrice', 25, 2)->unsigned();
			$table->integer('inventory_id')->unsigned();
			$table->integer('purchase_id')->unsigned();
			$table->timestamps();

			$table->index(['id', 'inventory_id', 'purchase_id']);

			$table->foreign('inventory_id')->references('id')->on('inventories')->onDelete('restrict')->onUpdate('cascade');
			$table->foreign('purchase_id')->references('id')->on('purchases')->onDelete('restrict')->onUpdate('cascade');
		});
	}

	public function down()	{
		Schema::dropIfExists('inventory_purchases');
	}

}
