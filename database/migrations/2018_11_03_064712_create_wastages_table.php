<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWastagesTable extends Migration	{

	public function up()	{
		Schema::create('wastages', function (Blueprint $table) {
			$table->increments('id');
			$table->text('description')->nullable();
			$table->integer('client_id')->unsigned();
			$table->integer('sale_id')->unsigned();
			$table->timestamps();

			$table->index(['id', 'client_id', 'sale_id']);

			$table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('sale_id')->references('id')->on('sales')->onDelete('cascade')->onUpdate('cascade');
		});
	}
	public function down()	{
		Schema::dropIfExists('wastages');
	}
}
