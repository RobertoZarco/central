<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration	{

	public function up()	{
		Schema::create('clients', function (Blueprint $table) {
			$table->increments('id');
			$table->string('alias');
			$table->string('reason');
			$table->string('phone',10);
			$table->string('email')->nullable();
			$table->string('rfc',13)->nullable();
			$table->timestamps();
			$table->softDeletes();

			$table->index(['id', 'alias']);
		});
	}

	public function down()	{
		Schema::dropIfExists('clients');
	}

}