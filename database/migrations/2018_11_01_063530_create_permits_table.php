<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermitsTable extends Migration	{

	public function up()	{
		Schema::create('permits', function (Blueprint $table) {
			$table->increments('user_id');
			$table->boolean('buys')->default(0);
			$table->boolean('cashClosings')->default(0);
			$table->boolean('salesQuotations')->default(0);
			$table->boolean('wastages')->default(0);
			$table->boolean('expenses')->default(0);
			$table->boolean('clientsProviders')->default(0);
			$table->boolean('credits')->default(0);
			$table->boolean('reports')->default(0);
			$table->boolean('inventory')->default(0);
			$table->boolean('accounting')->default(0);
			$table->boolean('systemUsers')->default(0);
			$table->boolean('banks')->default(0);
			$table->timestamps();

			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade'); //restrict, cascade, set null

			$table->index([ 'user_id']);
		});
	}

	public function down()	{
		Schema::dropIfExists('permits');
	}

}