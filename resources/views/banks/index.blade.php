@extends('adminlte::layouts.app')

@section('htmlheader_title', 'bancos')
@section('contentheader_title','Bancos')
@section('contentheader_description','DescBancos')

@section('main-content')
<div class="container-fluid spark-screen">
  <div class="row">
    <div class="col-md-9 col-md-offset-1">
      <div class="box box-primary box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Listado de Bancos</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
         @if (!empty($success) || session("success"))
         <div class="alert alert-success" role="alert">
          <strong>{{ $success ?? session('success') }}</strong>
        </div>
        @endif
        <div class="col-xs-12">
          <table id="mi-tabla" class="table table-bordered table-condensed mi-tabla table-hover table-striped">
            <thead>
              <tr>
                <th>Id</th>
                <th>Razon Social</th>
                <th>RFC</th>
                <th>Editar</th>
                <th>Borrar</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($banks as $banco)
              <tr>
                <td>{{ $banco->id}}</td>
                <td>{{ $banco->reason }}</td>
                <td>{{ $banco->rfc }}</td>
                <td><a class="btn btn-primary btn-xs" href=" {{action('BankController@edit',$banco->id)}}"><span class="glyphicon glyphicon-pencil"></span></a></td>
                <td>
                  <form action="{{action('BankController@destroy', $banco->id)}}" method="post">
                   @csrf
                   @method('DELETE')
                   <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                 </td>
               </tr>
             </form>
             @endforeach
           </tbody>
         </table>
       </div>
       <a class="btn btn-primary" href=" {{route('banks.create')}}" >Agregar banco</a>
     </div>
     <!-- /.box-body -->
   </div>
 </div>
</div>
</div>
@endsection

@section('estilos')
<style>
</style>
@endsection

@section('javascripts')
<script>
  $(function() {
    var table = $(".mi-tabla").DataTable({
     "pagingType": "simple_numbers",
     order: [1,"asc"],
     lengthMenu: [[10, 15, 30, -1], [10, 15, 30, "Todos"]],
     dom: '<lf<t>ip>',
     language: {
      "lengthMenu": "Ver _MENU_ registros por página",
      "zeroRecords": "No se encontraron registros para ese filtro",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoEmpty": "No hay registros disponibles",
      "search":     "Buscar:",
      "infoFiltered": "(Filtrados de _MAX_ totales)",
      "loadingRecords": "Cargando...",
      "processing":     "Procesando...",
      "paginate": {
        "first":      "Primera",
        "last":       "Última",
        "next":       "Siguiente",
        "previous":   "Anterior"
      },

    }

  });
  });
</script>
@endsection
