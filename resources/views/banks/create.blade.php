@extends('adminlte::layouts.app')

@section('htmlheader_title', 'Agregar Banco')
@section('contentheader_title','Bancos')
@section('contentheader_description','Agregar banco')

@section('main-content')
<div class="container-fluid spark-screen">
  <div class="row">
     <div class="col-md-6 col-md-offset-1">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Formulario para agregar un banco</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="box-body">
                <div class="row-">
                  <div class="col-xs-12">

                    <form action=" {{ route('banks.store') }} " method="POST" class="form-horizontal" role="form">
                      {{ csrf_field() }}
                      <div class="form-group">
                        <legend>Agregar Banco</legend>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="">Razon Social</label>
                        <input type="text" class="form-control" id="" name="reason" placeholder="Razon Social" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="">RFC</label>
                        <input type="text" class="form-control" id="" name="rfc" placeholder="RFC" maxlength ="13" required>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                          <button type="submit" class="btn btn-primary">Guardar</button>
                          <a href="{{ url("/banks") }}" class="btn btn-primary">Regresar</a>
                      </div>
                  </div>
              </form>
          </div>
      </div>
  </div>
</div>
<!-- /.box-body -->
</div>
</div>
</div>
</div>
@endsection
