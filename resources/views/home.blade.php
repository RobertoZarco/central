@extends('adminlte::layouts.app')

@section('htmlheader_title', 'Inicio')
@section('contentheader_title','Inicio')

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Bienvenido a {{ config('app.name').", ".ucwords(mb_strtolower(Auth::user()->username)) }}</h3>
						<h3 class="box-title pull-right">{{ ucfirst(Date::now()->format('l j \d\e F \d\e Y.')) }}</h3>
					</div>

					<div class="box-body">
						{{ trans('adminlte_lang::message.logged') }}.
					</div>
				</div>

			</div>
		</div>
	</div>
@endsection

@section('estilos')
	<style>
	</style>
@endsection

@section('javascripts')
	<script>
		$(function() {

		});
	</script>
@endsection