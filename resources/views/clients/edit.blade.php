@extends('adminlte::layouts.app')

@section('htmlheader_title', 'Editar cliente/proveedor '.$client->alias)
@section('contentheader_title', 'Editar cliente/proveedor: "'.$client->alias.'"')

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Formulario</h3>
						<h3 class="box-title pull-right">{{ ucfirst(Date::now()->format('l j \d\e F \d\e Y.')) }}</h3>
					</div>

					<div class="box-body row">
						@if(isset($success) || session('success'))
							<div class="col-md-10 col-md-offset-1 alert alert-success" role="alert">
								<p>{!! $success ?? session('success') !!}</p>
							</div>
						@endif
						<div class="col-md-6 col-md-offset-3">
							<form method="POST" action="{{ url()->current() }}">
								@csrf
								@method('patch')
								<div class="form-group {{ $errors->has('alias') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="alias">
												{!! $errors->has('alias') ? "<i class='fas fa-times'></i>" : '' !!} Apodo/Pseudónimo:
											</label>
										</div>
										<div class="col-md-8">
											<input type="text" class="form-control" id="alias" name="alias" value="{{ old('alias') ?? $client->alias }}">
										</div>
									</div>
									{!! $errors->has('alias') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('alias')."</span></div>" : '' !!}
								</div>
								<div class="form-group {{ $errors->has('reason') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="reason">
												{!! $errors->has('reason') ? "<i class='fas fa-times'></i>" : '' !!} Razón Social o Nombre:
											</label>
										</div>
										<div class="col-md-8">
											<input type="text" class="form-control" id="reason" name="reason" value="{{ old('reason') ?? $client->reason }}">
										</div>
									</div>
									{!! $errors->has('reason') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('reason')."</span></div>" : '' !!}
								</div>
								<div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="phone">
												{!! $errors->has('phone') ? "<i class='fas fa-times'></i>" : '' !!} Teléfono fijo:
											</label>
										</div>
										<div class="col-md-8">
											<input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') ?? $client->phone }}">
										</div>
									</div>
									{!! $errors->has('phone') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('phone')."</span></div>" : '' !!}
								</div>
								<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="email">
												{!! $errors->has('email') ? "<i class='fas fa-times'></i>" : '' !!} Correo electrónico: <small>[Opcional]</small>
											</label>
										</div>
										<div class="col-md-8">
											<input type="text" class="form-control" id="email" name="email" value="{{ old('email') ?? $client->email }}">
										</div>
									</div>
									{!! $errors->has('email') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('email')."</span></div>" : '' !!}
								</div>
								<div class="form-group {{ $errors->has('rfc') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="rfc">
												{!! $errors->has('rfc') ? "<i class='fas fa-times'></i>" : '' !!} RFC: <small>[Opcional]</small>
											</label>
										</div>
										<div class="col-md-8">
											<input type="rfc" class="form-control" id="rfc" name="rfc" value="{{ old('rfc') ?? $client->rfc }}">
										</div>
									</div>
									{!! $errors->has('rfc') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('rfc')."</span></div>" : '' !!}
								</div>
								<div class="text-right">
									<a href="{{ url("/clients") }}" class="pull-left btn btn-flat btn-warning">Regresar</a>
									<button type="submit" class="pull-right btn btn-flat btn-success">Actualizar cliente/proveedor</button>
								</div>
							</form>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
@endsection

@section('estilos')
	<style>
		div.box{
			padding-bottom: 5rem;
		}
		div.row.flex {
			display: flex;
			flex-direction: row;
		}
		div.row.flex > [class^="col-"], div.row.flex > [class*=" col-"] {
			display: flex;
			align-items: center;
			justify-content: right;
		}
		div.row.flex label.control-label {
			margin: 0;
			text-align: right;
		}
		label.btn{
			display: inline-table;
			margin-bottom: 10px;
		}
		label.btn span{
			display: table-cell;
			vertical-align: middle;
		}
	</style>
@endsection

@section('javascripts')
	<script>
		$(function() {
			
		});
	</script>
@endsection