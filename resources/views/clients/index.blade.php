@extends('adminlte::layouts.app')
@section('htmlheader_title', 'Clientes/Proveedores')
@section('contentheader_title','Ver clientes/proveedores')

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">

				<div class="box box-purple">
					<div class="box-header with-border">
						<h3 class="box-title">Listado clientes/proveedores</h3>
						<h3 class="box-title pull-right">{{ ucfirst(Date::now()->format('l j \d\e F \d\e Y.')) }}</h3>
					</div>
					<div class="box-body row">
						@if (!empty($success) || session("success"))
							<div class="col-md-10 col-md-offset-1 alert alert-success" role="alert">
								<p>{!! $success ?? session('success') !!}</p>
							</div>
						@endif
						@if(isset($info) || session('info'))
							<div class="col-md-10 col-md-offset-1 alert alert-info" role="alert">
								<p>{!! $info ?? session('info') !!}</p>
							</div>
						@endif
						<div class="col-md-12 text-center">
							<a href="/clients/create" class="btn btn-flat btn-primary">Registrar cliente</a>
						</div>
						<div class="col-md-12">
							<table class="datatable">
								<thead>
									<tr>
										<th>Alias</th>
										<th>Nombre</th>
										<th>Teléfono</th>
										<th>Correo Electrónico</th>
										<th>RFC</th>
										<th>Status</th>
										<th>Acciones</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($clients as $client)
										<tr>
											<td>{{ $client->alias }}</td>
											<td>{{ $client->reason }}</td>
											<td>{{ $client->phone }}</td>
											<td>{{ $client->email }}</td>
											<td>{{ $client->rfc }}</td>
											@if(!$client->trashed())
												<td class="text-green">Activado {{ $client->created_at->diffForHumans() }}</td>
											@else
												<td class="text-red">Desactivado {{ ucfirst($client->deleted_at->diffForHumans()) }}</td>
											@endif
											<td>
												<form method="post" action="{{ url()->current()."/$client->id" }}">
													@csrf
													@method('DELETE')
													@if(!$client->trashed())
														<a href="{{ url()->current()."/$client->id" }}" class="btn btn-success" data-toggle="tooltip"><i class="fas fa-user-edit"></i></a>
														<button type="button" class="btn btn-danger" data-toggle="confirmation"><i class="fas fa-user-times"></i></button>
													@else
														<a href="#" class="btn btn-info" data-toggle="confirmation"><i class="fas fa-user-check"></i></a>
													@endif
												</form>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
@endsection

@section('estilos')
	<style>
		table.datatable td, table.datatable th{
			padding: 5px;
		}
		table.datatable, div.dataTables_scrollHeadInner{
			margin: auto;
		}
	</style>
@endsection

@section('javascripts')
	<script>
		$(function() {
			$('a.btn-success[data-toggle=tooltip]').tooltip({
				container		: 'body',
				placement		: 'left',
				title				: 'Editar',
			});

			$('button.btn-danger[data-toggle=confirmation]').confirmation({
				rootSelector: '[data-toggle=confirmation]',
				container		: 'body',
				title				: 'Eliminar',
				content			: '¿Seguro que deseas eliminar este usuario?',
				popout			: true,
				singleton		: true,
				placement		: 'left',
				onConfirm: function(value) {
					$(this).parent().submit();
				},
				buttons: [
					{
						class	:'btn btn-danger',
						icon	:'fa fa-trash',
						label	:'Eliminar',
					},{
						class	:'btn btn-default',
						icon	:'fa fa-minus-square',
						label	:'Cancelar',
						cancel:true,
					},
				]
			});
			$('a.btn-info[data-toggle=confirmation]').confirmation({
				rootSelector: '[data-toggle=confirmation]',
				container		: 'body',
				title				: 'Reactivar',
				content			: '¿Seguro que deseas activar este usuario?',
				popout			: true,
				singleton		: true,
				placement		: 'left',
				onConfirm: function(value) {
					$(this).parent().submit();
				},
				buttons: [
					{
						class	:'btn btn-danger',
						icon	:'fa fa-trash',
						label	:'Activar',
					},{
						class	:'btn btn-default',
						icon	:'fa fa-minus-square',
						label	:'Cancelar',
						cancel:true,
					},
				]
			});
			var table = $("table.datatable").DataTable({
				order: [],
				scrollX: true,
				columnDefs: [
					{targets: "_all", "className": "text-center"},
					{targets: [-1, -2], "orderable": false},
				],
				lengthMenu: [[10, 15, 30, -1], [10, 15, 30, "Todos"]],
				language: {
					"lengthMenu": "Ver _MENU_ clientes por página",
					"zeroRecords": "No se encontraron clientes para ese filtro",
					"info": "Mostrando página _PAGE_ de _PAGES_",
					"infoEmpty": "No hay clientes disponibles",
					"search":     "Buscar:",
					"infoFiltered": "(Filtrados de _MAX_ totales)",
					"loadingRecords": "Cargando...",
					"processing":     "Procesando...",
					"paginate": {
						"first":      "Primera",
						"last":       "Última",
						"next":       "Siguiente",
						"previous":   "Anterior"
					},				
				},
				initComplete: function(settings, json){
					$('select[name="DataTables_Table_0_length"]').select2({
						theme: "bootstrap",
						language: "es-MX",
					});
				}
			});
		});
	</script>
@endsection

