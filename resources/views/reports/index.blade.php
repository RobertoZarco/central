@extends('adminlte::layouts.app')
@section('htmlheader_title', 'Reportes')
@section('contentheader_title','Reportes')


@section('main-content')
 <link rel="stylesheet" href="{{ asset('css/graficas.css') }}">
<div class="container-fluid spark-screen">
  <div class="row">
   <div class="col-md-9 col-md-offset-1">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Reportes</h3>
        <h3 class="box-title pull-right">{{ ucfirst(Date::now()->format('l j \d\e F \d\e Y.')) }}</h3>
        <div class="box-tools pull-right">
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner">
                <h3>Ventas</h3>
                <p>Consultas de Ventas</p>
              </div>
              <div class="icon">
                <i class="fa fa-shopping-cart"></i>
              </div>
              <a href="{{route('/report/sales')}}" class="small-box-footer">
                Consultar <i class="fa fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner">
                <h3>Compras<sup style="font-size: 20px"></sup></h3>

                <p>Consultas de compras</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="{{route('/report/shops')}}" class="small-box-footer">
                Consultar <i class="fa fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3>Gastos</h3>

                <p>Consulta de gastos</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="{{route('/report/expenses')}}" class="small-box-footer">
                Consulta <i class="fa fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
              <div class="inner">
                <h3>Pagos</h3>

                <p>Consultas de creditos</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
             <a href="{{route('/report/payments')}}" class="small-box-footer">
                Consulta <i class="fa fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
        </div>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
</div>
<div class="container">
  <canvas id="myChart"></canvas>
</div>
  {{-- cambios ver --}}
    {{-- {{ dd($data) }} --}}
  @php
    //$gastos = int($data->gastos);
  @endphp
  {{-- cambios ver --}}
@endsection
@section('estilos')
<style>
</style>
@stop
@section('javascripts')
<script type="text/javascript" src="{{ URL::asset('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js') }}"></script>
<script>
  let myChart = document.getElementById('myChart').getContext('2d');

  let massPopChart = new Chart(myChart, {
    type:'bar',//bar,horizontalBar,pie,line, horizontalBar, radar, polarArea
    data:{
      labels:['Gastos','Ventas','Compras','Creditos'],
      datasets:[{
        label:'Grafica general',
        data: [
        {{$data->gastos   ?? "0" }},
        {{$data->ventas   ?? "0" }},
        {{$data->compras  ?? "0" }},
        {{$data->creditos ?? "0" }},
        ],
        backgroundColor:[
        '#00C0EC',
        '#F9982E',
        '#00A360',
        ],
        borderWidth:4,
        bordercolor: '#777',
        hoverBorderWidth: 3,
        hoverBorderColor:'black'
      }]
    },
    options:{}
  });
</script>
@endsection
