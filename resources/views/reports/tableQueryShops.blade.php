          <table class="table table-hover">
            <thead>
              <tr>
                <th>Cliente</th>
                <th>Tipo de Pago</th>
                <th>Fecha de Venta</th>
                <th>Fecha de Venta</th>
                <th>Precio de Venta</th>
              </tr>
            </thead>
            <tbody>
             @php
             $sum = 0;
             @endphp
             @foreach ($shops as $compras)
             <tr>
              <td>{{ $compras -> provider -> alias }}</td>
              <td>{{ $compras -> payment-> description}}</td>
              <td>{{ $compras-> created_at}}</td>
              @foreach($compras->inventories as $product)
              @php $sum += $product->inventory->salePrice @endphp
              <td>{{ $product->inventory->name}}</td>
              <td>{{ $product->inventory->salePrice}}</td>
              @endforeach
            </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th></th>
              <th></th>
              <th></th>
              <th>Total</th>
              <th>${{ $sum }}</th>
            </tr>
          </tfoot>
        </table>
