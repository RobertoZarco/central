@extends('adminlte::layouts.app')
@section('htmlheader_title', 'Reporte de Pagos')
@section('contentheader_title','Reporte de Pagos')
@section('main-content')
<div class="col-md-4">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Ingreso de datos para el reporte de Pagos</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar"><i class="fa fa-minus"></i></button>
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body row justify-content-center" style="padding-bottom: 40px;">
            <div class="col-md-10">
                <form id="frmDatosReporte" method="post" autocomplete="off">
                        <label>por cliente</label>
                    <div class="container">
                        <select name="client" id="client" name="client" class="custom-select" required="required">
                            <option value="" disabled="" selected="">Seleccione Cliente</option>
                            @foreach ($clients as $cliente)
                            <option value="{{ $cliente->id }}">{{ $cliente->reason }}</option>
                            @endforeach
                        </select>
                    </div>
                    <br>
                    <label>Por rango de fecha</label>
                    <div class="container">
                        <label>del:</label>
                        <input type="date" name="fecha1">
                        <label>al:</label>
                        <input type="date" name="fecha2">
                    </div>
                    <br>
                    <div class="text-center">
                        <button id="sendData" type="button" class="btn btn-sm btn-primary">Consultar</button>
                    </div>
                    <a class="btn btn-success" href="{{route('Reportindex')}}" role="button">Regresar</a>
                </form>

            </div>
        </div>
    </div>
</div>

<div class="col-md-8">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Resultado de la consulta: </h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar"><i class="fa fa-minus"></i></button>
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body row justify-content-center" style="padding-bottom: 40px;">
            <div id="loader" class="col-md-12 text-center">
                <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
            </div>
            <div class="col-md-12" id="dataReport">

            </div>
        </div>
    </div>
</div>
@stop
