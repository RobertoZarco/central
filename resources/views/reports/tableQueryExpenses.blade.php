<table class="table">
  <thead>
    <tr>
      <th>Descripcion</th>
      <th>Tipo de Pago</th>
      <th>Fecha</th>
      <th>Cantidad</th>
    </tr>
  </thead>
  <tbody>
    @php
      $sum = 0;
    @endphp
    @foreach ($expenses as $expense) 
    @php
      $sum += $expense->amount;
    @endphp
    <tr>
      <td>{{ $expense->description }}</td>
      <td>{{ $expense->paymentType->description}}</td>
      <td>{{ $expense->created_at }}</td>
      <td>{{ $expense->amount }}</td>
    </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <th></th>
      <th></th>
      <th>Total</th>
      <th>{{ $sum }}</th>
    </tr>
  </tfoot>
</table>