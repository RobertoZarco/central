          <table class="table table-hover">
            <thead>
              <tr>
                <th>id venta</th>
                <th>Producto</th>
                <th>Tipo de Pago</th>
                <th>Cantidad</th>
                <th>Precio de Venta</th>
                <th>cliente</th>
                <th>Fecha de Venta</th>
                <th>Total</th>
              </tr>
            </thead>
            <tbody>
             @php
             $sum = 0;
             @endphp
             @foreach ($sales as $ventas)
             @php
             //$sum += $ventas->salePrice;
             @endphp
               @foreach($ventas->inventories as $product)
               @php $sum += $product->amount*$product->inventory->salePrice;  @endphp
             <tr>
              <th>{{ $ventas->id}}</th>
              <td>{{ $product->inventory->name }}</td>
              <td>{{ $ventas -> payment->description}}</td>
              <td>{{ $product->amount}}</td>
              <td>${{ $product->inventory->salePrice }}</td>
              <td>{{ $ventas -> client->alias}}</td>
              <td>{{ $ventas -> created_at}}</td>
               {{-- {{dd($product)}} --}}
              <td>${{$product->amount*$product->inventory->salePrice}}</td>
              @endforeach
            </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>Total</th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th>${{ $sum }}</th>
            </tr>
          </tfoot>
        </table>
