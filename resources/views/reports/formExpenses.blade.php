  @extends('adminlte::layouts.app')
  @section('htmlheader_title', 'Reporte de Gastos')
  @section('contentheader_title','Reporte de Gastos')
  @section('main-content')
  <div class="col-md-4">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Ingreso de datos para el reporte de Gastos</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar"><i class="fa fa-minus"></i></button>
        </div><!-- /.box-tools -->
      </div><!-- /.box-header -->
      <div class="box-body row justify-content-center" style="padding-bottom: 40px;">
        <div class="col-md-10">
          <form id="FormQueryExpenses" method="POST" autocomplete="off">
           {{ csrf_field() }}
           <br>
           <label>Por rango de fecha</label>
           <div class="container">
            <label>del:</label>
            <input type="date" name="fecha1">
            <label>al:</label>
            <input type="date" name="fecha2">
          </div>
          <br>
          <div class="text-center">
            <button type="button" class="btn btn-sm btn-primary" id="getqueryExpenses">Consultar</button>
          <a class="btn btn-success" href="{{route('Reportindex')}}" role="button">Regresar</a>
          </div>
        </form>
      </div>
    </div>
      {{-- {{dd($expensescharts)}} --}}
      {{-- <canvas id="myChart"></canvas>   --}}
    </div>
    <div>
      <p></p>
    </div>
</div>
<div class="col-md-8">
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title pull-right">{{ ucfirst(Date::now()->format('l j \d\e F \d\e Y.')) }}</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body row justify-content-center" style="padding-bottom: 40px;">
      <div class="col-md-12" id="resultQueryExpenses">

      </div>
    </div>
  </div>
</div>
@stop
@section('estilos')

<style>
</style>
@stop
@section('javascripts')
<script type="text/javascript" src="{{ URL::asset('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js') }}"></script>
<script>
  $("#getqueryExpenses").click(function(){
   $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
   $.ajax({
     url: '{{ url("/report/getQueryExpenses") }}',
     type: 'POST',
     data: $("#FormQueryExpenses").serialize(),
   })
   .done(function(data) {
     $('#resultQueryExpenses').html(data).promise().done(function(){
      $('#resultQueryExpenses table').DataTable({
        dom: 'Blfrtip',
        buttons: [
        { 
          extend: 'copy',
          text: 'Copiar',
        },
        { 
          extend: 'excel',
          text: 'Exportar en Excel',
        },
        {
          extend: 'pdf',
          text: 'Exportar en Pdf',
        }
        ],
        "language": {
          "lengthMenu": "mostrar _MENU_ resultados por pagina",
          "search": "Buscar",
          "zeroRecords": "No existen datos para esta consulta",
          "info": "Mostrando pagina _PAGE_ de _PAGES_",
          "infoEmpty": "No existen datos para esta consulta",
          "infoFiltered": "(filtered from _MAX_ total records)",
          "paginate": {
            "first"   : "Primera",
            "last"    : "Última",
            "next"    : "Siguiente",
            "previous": "Anterior",
          },
        }
      });
    })
     .fail(function() {
       console.log("error");
     })
     .always(function() {
       console.log("complete");
     });
   });
 });

  let myChart = document.getElementById('myChart').getContext('2d');

  let massPopChart = new Chart(myChart, {
    type:'bar',//bar,horizontalBar,pie,line, horizontalBar, radar, polarArea
    data:{
      labels:['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre'],
      datasets:[{
        label:'Grafica general',
        data: [
        895648,
        554546,
        445656,
        454565,
        ],
        backgroundColor:[
        '#00C0EC',
        '#F9982E',
        '#00A360',
        ],
        borderWidth:4,
        bordercolor: '#777',
        hoverBorderWidth: 3,
        hoverBorderColor:'black'
      }]
    },
    options:{}
  });

</script>
@endsection