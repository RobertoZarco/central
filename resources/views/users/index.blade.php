@extends('adminlte::layouts.app')

@section('htmlheader_title', 'Ver usuarios')
@section('contentheader_title','Usuarios')

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">

				<!-- Default box -->
				<div class="box box-purple">
					<div class="box-header with-border">
						<h3 class="box-title">Usuarios</h3>
						<h3 class="box-title pull-right">{{ ucfirst(Date::now()->format('l j \d\e F \d\e Y.')) }}</h3>
					</div>
					<div class="box-body row">
						@if(isset($success) || session('success'))
							<div class="col-md-10 col-md-offset-1 alert alert-success" role="alert">
								<p>{!! $success ?? session('success') !!}</p>
							</div>
						@endif
						@if(isset($info) || session('info'))
							<div class="col-md-10 col-md-offset-1 alert alert-info" role="alert">
								<p>{!! $info ?? session('info') !!}</p>
							</div>
						@endif
						<div class="col-md-12 text-center">
							<a href="/users/create" class="btn btn-flat btn-primary">Registrar usuario</a>
						</div>
						<div class="col-md-12">
							<table class="datatable">
								<thead>
									<tr>
										<th>Usuario</th>
										<th>Nombre</th>
										<th>Paterno</th>
										<th>Materno</th>
										<th>Teléfono Fijo</th>
										<th>Teléfono Celular</th>
										<th>Email</th>
										<th>Creado/Eliminado</th>
										<th>Acciones</th>
									</tr>
								</thead>
								<tbody>
									{{-- {{dd($users)}} --}}
									@foreach($users as $user)
										<tr>
											<td>{{ $user->username }}</td>
											<td>{{ mb_strtoupper($user->name) }}</td>
											<td>{{ mb_strtoupper($user->paternal) }}</td>
											<td>{{ mb_strtoupper($user->maternal) }}</td>
											<td>{{ $user->phone }}</td>
											<td>{{ $user->mobile }}</td>
											<td>{{ $user->email }}</td>
											@if(!$user->trashed())
												<td class="text-green">Activado {{ $user->created_at->diffForHumans() }}</td>
											@else
												<td class="text-red">Desactivado {{ ucfirst($user->deleted_at->diffForHumans()) }}</td>
											@endif
											<td>
												@if(!$user->trashed())
													<form method="post" action="{{ url()->current()."/$user->id" }}">
														@csrf
														@method('DELETE')
														<a href="{{ url()->current()."/$user->id" }}" class="btn btn-success" data-toggle="tooltip"><i class="fa fa-pencil-square-o"></i></a>
														<a href="#" class="btn btn-danger" data-toggle="confirmation"><i class="fa fa-user-times"></i></a>
													</form>
												@else
													<form method="post" action="{{ url()->current()."/$user->id" }}">
														@csrf
														@method('DELETE')
														<a href="#" class="btn btn-info" data-toggle="confirmation"><i class="fas fa-user-check"></i></a>
													</form>
												@endif
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection

@section('estilos')
	<style>
		table.datatable td, table.datatable th{
			padding: 5px;
		}
		table.datatable, div.dataTables_scrollHeadInner{
			margin: auto;
		}
	</style>
@endsection

@section('javascripts')
	<script>
		$(function() {
			$('a.btn-success[data-toggle=tooltip]').tooltip({
				container		: 'body',
				placement		: 'left',
				title				: 'Editar',
			});

			$('a.btn-danger[data-toggle=confirmation]').confirmation({
				rootSelector: '[data-toggle=confirmation]',
				container		: 'body',
				title				: 'Eliminar',
				content			: '¿Seguro que deseas eliminar este usuario?',
				popout			: true,
				singleton		: true,
				placement		: 'left',
				onConfirm: function(value) {
					$(this).parent().submit();
				},
				buttons: [
					{
						class	:'btn btn-danger',
						icon	:'fas fa-user-slash',
						label	:'Eliminar',
					},{
						class	:'btn btn-default',
						icon	:'fas fa-times-circle',
						label	:'Cancelar',
						cancel:true,
					},
				]
			});
			$('a.btn-info[data-toggle=confirmation]').confirmation({
				rootSelector: '[data-toggle=confirmation]',
				container		: 'body',
				title				: 'Reactivar',
				content			: '¿Seguro que deseas activar este usuario?',
				popout			: true,
				singleton		: true,
				placement		: 'left',
				onConfirm: function(value) {
					$(this).parent().submit();
				},
				buttons: [
					{
						class	:'btn btn-info',
						icon	:'fas fa-user-check',
						label	:'Activar',
					},{
						class	:'btn btn-default',
						icon	:'fas fa-times-circle',
						label	:'Cancelar',
						cancel:true,
					},
				]
			});
			$("table.datatable").DataTable({
				order: [],
				scrollX: true,
				columnDefs: [
					{targets: "_all", "className": "text-center"},
					{targets: [-1, -2], "orderable": false},
				],
				lengthMenu: [[2, 5, 10, -1], [2, 5, 10, "Todos"]],
				language: {
					"lengthMenu"		: "Mostrar _MENU_ usuarios por página",
					"zeroRecords"		: "No se encontraron usuarios",
					"info"					: "Mostrando página _PAGE_ de _PAGES_",
					"infoEmpty"			: "No hay usuarios disponibles",
					"search"				: "Buscar:",
					"infoFiltered"	: "(Filtrados de _MAX_ totales)",
					"loadingRecords": "Cargando...",
					"processing"		: "Procesando...",
					"paginate": {
						"first"		: "Primera",
						"last"		: "Última",
						"next"		: "Siguiente",
						"previous": "Anterior"
					},
				},
				initComplete: function(settings, json){
					$('select[name="DataTables_Table_0_length"]').select2({
						theme: "bootstrap",
						language: "es-MX",
					});
				}
			});
		});
	</script>
@endsection