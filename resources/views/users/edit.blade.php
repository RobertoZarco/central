@extends('adminlte::layouts.app')

@section('htmlheader_title', 'Editar usuario '.$user->username)
@section('contentheader_title', 'Editar usuario: "'.$user->username.'"')

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Formulario</h3>
						<h3 class="box-title pull-right">{{ ucfirst(Date::now()->format('l j \d\e F \d\e Y.')) }}</h3>
					</div>

					<div class="box-body row">
						@if(isset($success) || session('success'))
							<div class="col-md-10 col-md-offset-1 alert alert-success" role="alert">
								<p>{!! $success ?? session('success') !!}</p>
							</div>
						@endif
						<div class="col-md-6 col-md-offset-3">
							<div class="text-center"><h4>Creado {{ $user->created_at->diffForHumans() }}</h4></div>
							<form method="POST" action="{{ url()->current() }}">
								@csrf
								@method('patch')
								<div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="username">
												{!! $errors->has('username') ? "<i class='fas fa-times'></i>" : '' !!} Nombre de usuario:
											</label>
										</div>
										<div class="col-md-8">
											<input type="text" class="form-control" id="username" name="username" value="{{ old('username') ?? $user->username }}">
										</div>
									</div>
									{!! $errors->has('username') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('username')."</span></div>" : '' !!}
								</div>
								<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="name">
												{!! $errors->has('name') ? "<i class='fas fa-times'></i>" : '' !!} Nombre(s): 
											</label>
										</div>
										<div class="col-md-8">
											<input type="text" class="form-control" id="name" name="name" value="{{ old('name') ?? $user->name }}">
										</div>
									</div>
									{!! $errors->has('name') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('name')."</span></div>" : '' !!}
								</div>
								<div class="form-group {{ $errors->has('paternal') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="paternal">
												{!! $errors->has('paternal') ? "<i class='fas fa-times'></i>" : '' !!} Apellido Paterno: 
											</label>
										</div>
										<div class="col-md-8">
											<input type="text" class="form-control" id="paternal" name="paternal" value="{{ old('paternal') ?? $user->paternal }}">
										</div>
									</div>
									{!! $errors->has('paternal') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('paternal')."</span></div>" : '' !!}
								</div>
								<div class="form-group {{ $errors->has('maternal') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="maternal">
												{!! $errors->has('maternal') ? "<i class='fas fa-times'></i>" : '' !!} Apellido Materno: <small>[Opcional]</small>
											</label>
										</div>
										<div class="col-md-8">
											<input type="text" class="form-control" id="maternal" name="maternal" value="{{ old('maternal') ?? $user->maternal }}">
										</div>
									</div>
									{!! $errors->has('maternal') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('maternal')."</span></div>" : '' !!}
								</div>
								<div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="phone">
												{!! $errors->has('phone') ? "<i class='fas fa-times'></i>" : '' !!} Teléfono fijo: <small>[Opcional]</small>
											</label>
										</div>
										<div class="col-md-8">
											<input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') ?? $user->phone }}">
										</div>
									</div>
									{!! $errors->has('phone') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('phone')."</span></div>" : '' !!}
								</div>
								<div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="mobile">
												{!! $errors->has('mobile') ? "<i class='fas fa-times'></i>" : '' !!} Teléfono celular: <small>[Opcional]</small>
											</label>
										</div>
										<div class="col-md-8">
											<input type="text" class="form-control" id="mobile" name="mobile" value="{{ old('mobile') ?? $user->mobile }}">
										</div>
									</div>
									{!! $errors->has('mobile') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('mobile')."</span></div>" : '' !!}
								</div>
								<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="email">
												{!! $errors->has('email') ? "<i class='fas fa-times'></i>" : '' !!} Correo electrónico: <small>[Opcional]</small>
											</label>
										</div>
										<div class="col-md-8">
											<input type="text" class="form-control" id="email" name="email" value="{{ old('email') ?? $user->email }}">
										</div>
									</div>
									{!! $errors->has('email') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('email')."</span></div>" : '' !!}
								</div>
								<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="password">
												{!! $errors->has('password') ? "<i class='fas fa-times'></i>" : '' !!} Contraseña: <small>[Opcional]</small>
											</label>
										</div>
										<div class="col-md-8">
											<input type="password" class="form-control" id="password" name="password">
										</div>
									</div>
									{!! $errors->has('password') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('password')."</span></div>" : '' !!}
								</div>
								<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="password_confirmation">
												{!! $errors->has('password') ? "<i class='fas fa-times'></i>" : '' !!} Confirma contraseña: <small>[Opcional]</small>
											</label>
										</div>
										<div class="col-md-8">
											<input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
										</div>
									</div>
									{!! $errors->has('password') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('password')."</span></div>" : '' !!}
								</div>
								@if($user->isNot(Auth::user()))
									<div class="form-group text-center">
										<label type="button" class="btn btn-flat btn-primary"><input type="checkbox" name="buys" {{ (@$user->permits->buys == 1 && empty($errors->all())) || old('buys') == 'on' ? 'checked' : '' }}>&nbsp;<span>Compras</span></label>
										<label type="button" class="btn btn-flat btn-primary"><input type="checkbox" name="cashClosings" {{ (@$user->permits->cashClosings == 1 && empty($errors->all())) || old('cashClosings') == 'on' ? 'checked' : '' }}>&nbsp;<span>Corte</span></label>
										<label type="button" class="btn btn-flat btn-primary"><input type="checkbox" name="salesQuotations" {{ (@$user->permits->salesQuotations == 1 && empty($errors->all())) || old('salesQuotations') == 'on' ? 'checked' : '' }}>&nbsp;<span>Ventas/Cotizaciones</span></label>
										<label type="button" class="btn btn-flat btn-primary"><input type="checkbox" name="wastages" {{ (@$user->permits->wastages == 1 && empty($errors->all())) || old('wastages') == 'on' ? 'checked' : '' }}>&nbsp;<span>Devoluciones/Merma</span></label>
										<label type="button" class="btn btn-flat btn-primary"><input type="checkbox" name="expenses" {{ (@$user->permits->expenses == 1 && empty($errors->all())) || old('expenses') == 'on' ? 'checked' : '' }}>&nbsp;<span>Gastos</span></label>
										<label type="button" class="btn btn-flat btn-primary"><input type="checkbox" name="clientsProviders" {{ (@$user->permits->clientsProviders == 1 && empty($errors->all())) || old('clientsProviders') == 'on' ? 'checked' : '' }}>&nbsp;<span>Clientes/Proveedores</span></label>
										<label type="button" class="btn btn-flat btn-primary"><input type="checkbox" name="credits" {{ (@$user->permits->credits == 1 && empty($errors->all())) || old('credits') == 'on' ? 'checked' : '' }}>&nbsp;<span>Créditos</span></label>
										<label type="button" class="btn btn-flat btn-primary"><input type="checkbox" name="reports" {{ (@$user->permits->reports == 1 && empty($errors->all())) || old('reports') == 'on' ? 'checked' : '' }}>&nbsp;<span>Reportes</span></label>
										<label type="button" class="btn btn-flat btn-primary"><input type="checkbox" name="inventory" {{ (@$user->permits->inventory == 1 && empty($errors->all())) || old('inventory') == 'on' ? 'checked' : '' }}>&nbsp;<span>Inventario</span></label>
										<label type="button" class="btn btn-flat btn-primary"><input type="checkbox" name="accounting" {{ (@$user->permits->accounting == 1 && empty($errors->all())) || old('accounting') == 'on' ? 'checked' : '' }}>&nbsp;<span>Contaduría</span></label>
										<label type="button" class="btn btn-flat btn-primary"><input type="checkbox" name="systemUsers" {{ (@$user->permits->systemUsers == 1 && empty($errors->all())) || old('systemUsers') == 'on' ? 'checked' : '' }}>&nbsp;<span>Usuarios</span></label>
										<label type="button" class="btn btn-flat btn-primary"><input type="checkbox" name="banks" {{ (@$user->permits->banks == 1 && empty($errors->all())) || old('banks') == 'on' ? 'checked' : '' }}>&nbsp;<span>Bancos</span></label>
									</div>
								@endif
								
								<div class="text-right">
									@if($user->isNot(Auth::user()))
										<a href="{{ url("/users") }}" class="pull-left btn btn-flat btn-warning">Regresar</a>
									@else
										<a href="{{ url("/home") }}" class="pull-left btn btn-flat btn-warning">Regresar</a>
									@endif
									<button type="submit" class="pull-right btn btn-flat btn-success">Actualizar usuario</button>
								</div>
							</form>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
@endsection

@section('estilos')
	<style>
		div.box{
			padding-bottom: 5rem;
		}
		div.row.flex {
			display: flex;
			flex-direction: row;
		}
		div.row.flex > [class^="col-"], div.row.flex > [class*=" col-"] {
			display: flex;
			align-items: center;
			justify-content: right;
		}
		div.row.flex label.control-label {
			margin: 0;
			text-align: right;
		}
		.text-center{
			justify-content: center !important;
		}
		label.btn{
			display: inline-table;
			margin-bottom: 10px;
		}
		label.btn span{
			display: table-cell;
			vertical-align: middle;
		}
	</style>
@endsection

@section('javascripts')
	<script>
		$(function() {
			$("#password, #password_confirmation").tooltip({
				container		: 'body',
				placement		: 'top',
				title				: 'Llenar este campo solo si se desea cambiar la contraseña.',
			});
		});
	</script>
@endsection