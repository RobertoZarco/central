@extends('adminlte::layouts.app')

@section('htmlheader_title', 'TítuloPestaña')
@section('contentheader_title','TítuloCuerpo')
@section('contentheader_description','DescripciónCuerpo')

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Home</h3>
						<h3 class="box-title pull-right">{{ ucfirst(Date::now()->format('l j \d\e F \d\e Y.')) }}</h3>
						{{-- <div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i>
							</button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i>
							</button>
						</div> --}}
					</div>

					<div class="box-body">
						{{ trans('adminlte_lang::message.logged') }}.
					</div>
				</div>

			</div>
		</div>
	</div>
@endsection

@section('estilos')
	<style>
	</style>
@endsection

@section('javascripts')
	<script>
		$(function() {

		});
	</script>
@endsection