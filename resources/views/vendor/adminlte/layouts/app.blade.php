<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="es">
	@section('htmlheader')
	@include('adminlte::layouts.partials.htmlheader')
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/central.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/loader.css') }}">
	<link rel="icon" type="image/x-icon" href="{{ asset('/favicon.ico') }}" sizes="any">
	<style>
		aside.main-sidebar	{
			max-height: 100vh;
			overflow: auto;
		}
		span.select2-selection__rendered	{
			display: table-cell !important;
			vertical-align: middle;
		}
	</style>
	@yield('estilos')
	@show

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
	<body class="skin-purple sidebar-mini fixed">
		<div class="loaderWrapper">
			<div class="loader"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
		</div>
		<div id="app" v-cloak>
			<div class="wrapper">
				@include('adminlte::layouts.partials.mainheader')

				@include('adminlte::layouts.partials.sidebar')

				<!-- Content Wrapper. Contains page content -->
				<div class="content-wrapper">
					@include('adminlte::layouts.partials.contentheader')

					<!-- Main content -->
					<section class="content">
						<!-- Your Page Content Here -->
						@yield('main-content')
					</section><!-- /.content -->

				</div><!-- /.content-wrapper -->

				@include('adminlte::layouts.partials.controlsidebar')
				@include('adminlte::layouts.partials.footer')
			</div><!-- ./wrapper -->
		</div>
		@section('scripts')
		@include('adminlte::layouts.partials.scripts')
		@show
		<script>
			$(window).on('load',function(){
				$("div.loaderWrapper").hide();
			});
		</script>
		<script>
			//Corrección VMSC
			$(function() {
				$("aside.main-sidebar ul.sidebar-menu").find("li > a").each(function(index, el) {
					var liga = $(el).attr('href');
					var rutaActual = "{{ "/".implode("/", Request::segments()) }}";
					// console.log(el);
					if( rutaActual.indexOf(liga) >= 0){
						$(el).parent().addClass('active');
						if($(el).parent().parent().hasClass('treeview-menu'))
							$(el).parent().parent().parent().addClass('active');
					}
				});
				$("#user_menu a.dropdown-toggle").click(function(event) {
					$("#user_menu ul.dropdown-menu").toggle();
				});
				$("div.miniLoader").html("<div class='sk-cube-grid'><div class='sk-cube sk-cube1'></div><div class='sk-cube sk-cube2'></div><div class='sk-cube sk-cube3'></div><div class='sk-cube sk-cube4'></div><div class='sk-cube sk-cube5'></div><div class='sk-cube sk-cube6'></div><div class='sk-cube sk-cube7'></div><div class='sk-cube sk-cube8'></div><div class='sk-cube sk-cube9'></div></div>");
				$("div.miniLoader .sk-cube-grid").hide();
			});
		</script>    
		<script>
			$(function() {
				$('select.select2').select2({
					theme: "bootstrap",
					language: "es-MX",
				});
			});
		</script>
		@yield('javascripts')
	</body>
</html>