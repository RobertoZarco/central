<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		@yield('contentheader_title', 'Page Header here')
		<small>@yield('contentheader_description', Auth::user()->fullName)</small>
	</h1>
	{{-- <ol class="breadcrumb">
		<li><a href="#"><i class="fas fa-tachometer-alt"></i> {{ trans('adminlte_lang::message.level') }}</a></li>
		<li class="active">{{ trans('adminlte_lang::message.here') }}</li>
	</ol> --}}
</section>