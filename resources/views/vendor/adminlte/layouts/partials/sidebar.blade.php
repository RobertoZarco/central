<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">

		<!-- Sidebar user panel (optional) -->
		@if (! Auth::guest())
			<div class="user-panel">
				<div class="pull-left image">
					@if(Auth::user()->email)
						<img src="{{ Gravatar::get(Auth::user()->email) }}" class="img-circle" alt="User Image" />
					@else
						<img src="{{ Gravatar::get('example@example.com') }}" class="img-circle" alt="User Image" />
					@endif
				</div>
				<div class="pull-left info">
					<p>{{ Auth::user()->username }}</p>
					<!-- Status -->
					<a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
				</div>
			</div>
		@endif

		<!-- Sidebar Menu -->
		{{ Menu::sidebar() }}
		<!-- /.sidebar-menu -->
	</section>
	<!-- /.sidebar -->
</aside>