<!-- Main Footer -->
<footer class="main-footer">
	<!-- Default to the left -->
	<strong>Copyright &copy; 2018.</strong>
	<!-- To the right -->
	<div class="pull-right hidden-xs">
		<a href="https://github.com/acacha/adminlte-laravel"></a><b>{{ config('app.name') }}</b></a>.
	</div>
</footer>
