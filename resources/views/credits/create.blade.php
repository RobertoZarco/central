@extends('adminlte::layouts.app')

@section('htmlheader_title', 'Agregar Pagos')
@section('contentheader_title','Pagos')
@section('contentheader_description','Agregar Pago')

@section('main-content')
<div class="row">
    <section class="content">
        <div class="col-md-8 col-md-offset-2">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Error!</strong> Revise los campos obligatorios.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if(Session::has('success'))
            <div class="alert alert-info">
                {{Session::get('success')}}
            </div>
            @endif
            {{-- {{dd($users)}} --}}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Formulario para crear un Pago</h3>
                </div>
                <div class="panel-body">                    
                    <div class="table-container">
                        <form method="POST" action="{{ route('credits.store') }}"  role="form">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <label for="">Cliente</label>
                                        {{-- <select>
                                            @foreach($clients as $cliente)
                                            <option value="{{$cliente->id}}">{{ $cliente->alias}}</option>
                                            @endforeach
                                        </select>  --}}
                                        <select name="client_id" class="select2 form-control" required="required">
                                          <option value=""></option>
                                          @foreach ($clients as $cliente)                        
                                          <option value="{{ $cliente->id  }}">{{ $cliente->alias }}</option>
                                          @endforeach
                                      </select>
                                  </div>
                              </div>
                              <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                 <label for="">Usuario actual:</label>
                                 <label for="">{{ auth()->user()->username}}</label>
                             </div>
                         </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="">Cantidad</label>
                                <input type="number" min="0.00" max="10000.00" step="0.01" / name="amount" placeholder="Cantidad">
                                <input type="hidden" name="paid" value="0">
                                <input type="hidden" name="user_id" value="{{ auth()->user()->id}}">
                                <input type="hidden" name="paid_at" value="">
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <input type="submit"  value="Guardar" class="btn btn-success btn-block">
                            <a href="{{-- {{ route('libro.index') }} --}}" class="btn btn-info btn-block" >Atrás</a>
                        </div>  

                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
</section>
@endsection
