@extends('adminlte::layouts.app')

@section('htmlheader_title', 'Agregar Pagos')
@section('contentheader_title','Pagos')
@section('contentheader_description','Editar Pago')


@section('main-content')
{{-- <script type="text/javascript" src="{{ asset('../js/pagos.js')}}"></script> --}}
<div class="container-fluid spark-screen">
  <div class="row">
   <div class="col-md-9 col-md-offset-1">

    <div class="box box-success box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Editar</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        {{-- {{dd($periods->description)}} --}}
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Editar Credito Seleccionado</h3>
          </div>
          <div class="panel-body">          
            <div class="table-container">
              <div class="form-group">
                <label>Cliente: {{$credit->client->alias}} </label>
              </div>
              <form method="POST" action=" {{ route('credits.update',$credit->id) }} "  role="form">
                {{ csrf_field() }}
                <input name="_method" type="hidden" value="PATCH">
                <div class="row">
                  <div class="col-xs-6 col-sm-6 col-md-6">
                  </div>
                </div>
                <label for="">monto Actual</label>
                <label>${{$credit->amount}}</label>
                <div class="form-group">
                  <label for="">monto a pagar</label>
                  <input type="text" name="amount_payable">
                </div>
                <div class="row">

                  <div class="col-xs-12 col-sm-12 col-md-12">
                    <input type="submit"  value="Actualizar" class="btn btn-success btn-block">
                    <a href=" {{ route('credits.index') }}" class="btn btn-info btn-block" >Atrás</a>
                  </div>  

                </div>
              </form>
            </div>
          </div>

        </div>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
</div>
@endsection
@section('javascript')
@endsection
