@extends('adminlte::layouts.app')
@section('htmlheader_title', 'Pagos')
@section('contentheader_title','Pagos')


@section('main-content')
<div class="container-fluid spark-screen">
  <div class="row">
   <div class="col-md-9 col-md-offset-1">

    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Pagos</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="box-header with-border">
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
          <!-- /.box-tools -->
        </div>
        {{-- {{dd($credits)}} --}}
        <!-- /.box-header -->
        <div class="box-body">
         @if (!empty($success) || session("success"))
         <div class="alert alert-success" role="alert">
          <strong>{{ $success ?? session('success') }}</strong> 
        </div>
        @endif
        <div class="col-xs-12">
          <form action="">
            <table id="mi-tabla" class="table table-bordered table-condensed mi-tabla table-hover table-striped">
              <thead>
                <tr>
                  <th>Cliente</th>
                  <th>Deuda Actual</th>
                  <th>Status</th>
                  <th>Usuario</th>
                  <th>Fecha de creación</th>
                  <th>Editar</th>
                  {{-- <th>Borrar</th> --}}
                </tr>
              </thead>
              <tbody>
                @foreach ($credits as $credit)
                <tr>
                  <td>{{ $credit->client->alias }}</td>
                  <td>${{ $credit->amount }}</td>
                  <td>{{ $credit->pagado }}</td>
                  <td>{{ $credit->user->username }}</td>
                  <td>{{ $credit->created_at }}</td>
                  <td><a class="btn btn-primary btn-xs" href=" {{action('CreditController@edit',$credit->id)}}"><span class="glyphicon glyphicon-pencil"></span></a></td>
                 {{-- <td>
                  <form action="{{action('CreditController@destroy', $credit->id)}}" method="post">
                   @csrf
                   @method('DELETE')
                   <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                 </td> --}}
                </tr>
                  </form>
                @endforeach
              </tbody>
            </table>
        </div>
      </div>
      <a class="btn btn-primary" href=" {{route('credits.create')}}" >Agregar credito</a>
    </div>
    <!-- /.box-body -->
  </div>
</div>
</div>
</div>
@endsection
@section('estilos')
<style>
</style>
@endsection

@section('javascripts')
<script>
  $(function() {
    var table = $(".mi-tabla").DataTable({
     "tableTools": {
       "sRowSelect": "multi",
       "aButtons": [
       {
         "sExtends": "select_none",
         "sButtonText": "Borrar selección"
       }]
     },
     "pagingType": "simple_numbers",
     order: [1,"asc"],
     lengthMenu: [[10, 15, 30, -1], [10, 15, 30, "Todos"]],
     language: {
      "lengthMenu": "Ver _MENU_ pagos por página",
      "zeroRecords": "No se encontraron pagos para ese filtro",
      "info": "Mostrando página _PAGE_ de _PAGES_",
      "infoEmpty": "No hay pagos disponibles",
      "search":     "Buscar:",
      "infoFiltered": "(Filtrados de _MAX_ totales)",
      "loadingRecords": "Cargando...",
      "processing":     "Procesando...",
      "paginate": {
        "first":      "Primera",
        "last":       "Última",
        "next":       "Siguiente",
        "previous":   "Anterior"
      },

    }

  });
  });
</script>
@endsection


