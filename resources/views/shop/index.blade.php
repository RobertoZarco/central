@extends('adminlte::layouts.app')
@section('htmlheader_title', 'Compras')
@section('contentheader_title','Compras')

@section('main-content')
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Productos elegidos</h3>
					<h3 class="box-title pull-right">{{ ucfirst(Date::now()->format('l j \d\e F \d\e Y.')) }}</h3>
				</div>
				<div class="box-body">
					@if (!empty($success) || session("success"))
						<div class="alert alert-success" role="alert">
							<strong>{{ $success ?? session('success') }}</strong>
						</div>
					@endif

					<div class="row">
						<div id="productLoader" class="col-md-1 col-md-offset-3 miniLoader"></div>
						<div class="col-md-5">
							<div class="form-group has-feedback">
								<input type="text" placeholder="Buscar producto" id="productFind" name="productFind" class="form-control">
								<span class="icon fas fa-search-dollar form-control-feedback"></span>
								<div id="productList"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<table class="table datatable table-bordered table-hover table-striped">
								<thead>
									<tr>
										<th>Id</th>
										<th>Unidad</th>
										<th>Nombre</th>
										<th>Cantidad</th>
										<th>Categoria</th>
										<th>Precio unitario</th>
										<th>Acciones</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					<div class="row" style="padding-top: 3rem; display: flex; align-items: center;">
						<div class="col-md-3 col-md-offset-2 text-center">
							<label for="provider">Proveedor: </label>
							<select id="provider" class="select2">
								@foreach($clients as $client)
									<option value="{{ $client->id }}">{{ ucwords(mb_strtolower("\" ".$client->alias." \" ".$client->reason)) }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-2">
							<label for="paymentType">Tipo de pago: </label>
							<select id="paymentType" class="select2">
								@foreach($payments as $payment)
									<option value="{{ $payment->id }}">{{ ucwords(mb_strtolower($payment->key)) }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-3 text-right">
							<div class="row" style="display: flex; align-items: center;">
								<div class="col-md-4"><label style="margin-bottom: 0;" for="subTotalInput">Subtotal: </label></div>
								<div class="col-md-8"><input class="form-control" id="subTotalInput" type='text' value="$ 0.00" disabled></div>
							</div>
							<div class="row" style="display: flex; align-items: center; padding-top: 5px;">
								<div class="col-md-4"><label style="margin-bottom: 0;" for="discountInput">Descuento(%): </label></div>
								<div class="col-md-8"><input class="form-control" id="discountInput" type='number' min="0" max="100" value="0"></div>
							</div>
							<div class="row" style="display: flex; align-items: center; padding-top: 5px;">
								<div class="col-md-4"><label style="margin-bottom: 0;" for="totalInput">Total: </label></div>
								<div class="col-md-8"><input class="form-control" id="totalInput" type='text' value="$ 0.00" disabled></div>
							</div>
						</div>
					</div>
					<div class="row" style="padding-top: 3rem;">
						<div class="col-md-10 col-md-offset-1 text-right">
							<button id="saveShop" class="btn btn-info" data-toggle="modal" data-target="#myModal">Guardar compra</button>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">

			<div class="modal-content panel-info">
				<div class="modal-header panel-heading">
					<h4 class="modal-title">Guardar compra</h4>
				</div>

				<div class="modal-body">
					<div class="row" style="display: flex; align-items: center;">
						<div id="shopLoader" class="col-md-offset-4 col-md-2 miniLoader"></div>
						<div class="col-md-2">Guardando...</div>
					</div>
					<div class="row">
						<div id="modalContent" class="col-md-10 col-md-offset-1"></div>
					</div>
				</div>

				<div class="modal-footer">
					<button id="accept" type="button" class="btn btn-info" data-dismiss="modal">Aceptar</button>
				</div>
			</div>

		</div>
	</div>
@endsection
@section('estilos')
	<style>
		div.box-body {
			padding-bottom: 4rem;
		}
		table.datatable, div.dataTables_scrollHeadInner{
			margin: auto;
		}
		table.padding td{
			padding: 5px 0;
		}
		div.form-group > span.icon	{
			display: flex;
			justify-content: center;
		}
		div.form-group > span.icon:before {
			display: flex;
			align-items: center;
		}
		#productList > ul {
			min-width: 250px;
			max-height: 21.5rem;
			overflow: auto;
			display: block;
			position: absolute;
			width: -moz-available;
			width: -webkit-fill-available;
			width: fill-available;
		}
		.vertical_middle{
			vertical-align: middle !important;
		}
	</style>
@endsection

@section('javascripts')
	<script>
		var table				= null;
		var totalPrice	= 0;
		var discount		= 0;
		var products		= [];
		var prices			= [];
		$(function() {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$("#productFind").keyup(function(event) {
				var query = $(this).val();
				if(query.length >= 3){
					$.ajax({
						url: '{{ route("shops.filter") }}',
						type: 'patch',
						data: {query: query},
						beforeSend: function(){
							$("#productLoader > div").show();
						},
					}).done(function(data) {
						if(data != ''){
							$("#productList").html(data).promise().done(autocompleteListener());
						}	else {
							$("#productList").hide();
						}
					}).fail(function() {
						console.log("Error fetching autocomplete data");
					}).always(function(){
						$("#productLoader > div").hide();
					});

				}
			});
			// Only allow numbers
			$("#discountInput").parent().on('keydown', '#discountInput', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13])||(/65|67|86|88/.test(e.keyCode)&&(e.ctrlKey===true||e.metaKey===true))&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
			$("#discountInput").change(function(event) {
				if($(this).val() > 100)
					$(this).val(100);
				totalInputListener();
			});
			table = $(".datatable").DataTable({
				order: [],
				columnDefs: [
					{ name: 'id'},
					{ name: 'unit_name'},
					{ name: 'name'},
					{ name: 'amount'},
					{ name: 'category_name'},
					{ name: 'salePrice'},
					{ name: 'actions'},
					{ targets: '_all', className: 'vertical_middle'},
					{ targets: [-1], orderable: false},
				],
				searching: false,
				lengthMenu: [[10, 15, 30, -1], [10, 15, 30, "Todos"]],
				language: {
					"lengthMenu"		: "Ver _MENU_ productos por página",
					"zeroRecords"		: "No hay productos en la compra aún",
					"info"					: "Mostrando página _PAGE_ de _PAGES_",
					"infoEmpty"			: "No hay productos",
					"search"				: "Buscar:",
					"infoFiltered"	: "(Filtrados de _MAX_ totales)",
					"loadingRecords": "Cargando...",
					"processing"		: "Procesando...",
					"paginate": {
						"first"		: "Primera",
						"last"		: "Última",
						"next"		: "Siguiente",
						"previous": "Anterior"
					},
				},
				initComplete: function(settings, json){
					$('select[name="DataTables_Table_0_length"]').select2({
						theme: "bootstrap",
						language: "es-MX",
					});
				},
				drawCallback: function(settings){
					addActionsOnRow();
					totalInputListener();
				},

			});

			$("#saveShop").click(function(event) {
				$("#modalContent").html("");
				$("#shopLoader > div").show();
				$.ajax({
					url: '/shops',
					type: 'post',
					data: {products: products, discount: discount, provider: $("#provider").val(), paymentType: $("#paymentType").val()},
					beforeSend: function(){
						$("#shopLoader").parent().show();
					},
				}).done(function(data) {
					$("#modalContent").html(data.message);
					setTimeout(function(){
						var win = window.open('/shops/'+data.purchase_id, '_blank');
						win.focus();
					}, 1500);
					$("#accept").click(function(event) {
						location.reload(true);
					});
				}).fail(function(data) {
					alert("Error al intentar guardar.");
					console.log(data);
					$("#accept").unbind('click');
				}).always(function() {
					$("#shopLoader").parent().hide();
				});

			});
		});

		function autocompleteListener(){
			$("#productList > ul a").click(function(event) {
				let product = $(this).find("input[name='product']").val();
				$("#productFind").val("");
				event.preventDefault();
				$("#productList").hide();
				addItemOnList(product);
			});
			$("#productList").show();
		}

		function totalInputListener(){
			totalPrice = 0;
			$.each(products, function(index, val) {
				if(val != undefined)
					totalPrice += val*prices[index];
			});
			discount =  $("#discountInput").val();
			if(discount > 100)
				discount = 100;
			let subTotalPriceString	= (totalPrice).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
			let totalPriceString		= (totalPrice*(100-discount)/100).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
			$("#subTotalInput").val('$ '+subTotalPriceString);
			$("#totalInput").val('$ '+totalPriceString);
		}

		function addItemOnList(prod){
			if(products[prod] == undefined){
				$.ajax({
					url: '/shops/'+prod,
					type: 'post',
					dataType: 'json',
					beforeSend: function(){
						$("#productLoader > div").show();
					},
				})
				.done(function(data) {
					products[prod]	= 1;
					prices[prod]		= parseFloat(data.salePrice.replace(',', ''));
					data.amount			= 1;
					table.row.add([
						data.id,
						data.unit_name,
						data.name,
						data.amount,
						data.category_name,
						'$ '+data.salePrice,
						data.actions,
					]).draw(false);
				}).fail(function() {
					alert("Error al añadir el producto, intente de nuevo");
				}).always(function(){
					$("#productLoader > div").hide();
				});
			}	else 	{
				alert("Ya se encuentra ese elemento en la lista");
			}

		}

		function addActionsOnRow(){
			$('button.addAmount').unbind("click").click(function(event) {
				let row = $(this).parent().parent().parent();
				let id = $(row).find('td').eq(0).html();
				$(row).find('td').eq(3).html(++products[id]);
				totalInputListener();
			});
			$('button.substractAmount').unbind("click").click(function(event) {
				let row = $(this).parent().parent().parent();
				let id = $(row).find('td').eq(0).html();
				if(products[id] > 1)
					$(row).find('td').eq(3).html(--products[id]);
				totalInputListener();
			});
			$('button.deleteItem').unbind("click").click(function(event) {
				let rowToDelete = $(this).parent().parent();
				let id = $(rowToDelete).find('td').eq(0).html();
				products[id] = undefined;
				prices[id] = undefined;
				table.row(rowToDelete).remove().draw();
			});
		}
	</script>
@endsection


