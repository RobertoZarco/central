@if(!empty($items->toArray()))
	<ul class="dropdown-menu">
		@foreach($items as $item)
			<li>
				<a href="#">
					<span class="description">{{ ucfirst(mb_strtolower($item->unitType->key)) }} de <strong>{{ ucfirst(mb_strtolower($item->name)) }}</strong></span>
					<span class="pull-right">@moneda($item->salePrice)</span>
					<input type="hidden" name="product" value="{{ $item->id }}">
				</a>
			</li>
		@endforeach
	</ul>
@endif