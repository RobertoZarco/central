<div style="text-align: center;">
	<img src="{{ asset('logo.png') }}" style="height: 100px; width: auto;">
</div>
<br>
<div>
	<div style="float: left; width: 50%;">{{ config('app.name') }} &copy;{{ Date::now()->format('Y') }}.</div>
	<div style="float: right; width: 50%; text-align: right;">Compra No. <b>{{ str_pad($purchase->id, 9, '0', STR_PAD_LEFT) }}</b>.</div>
	<br>
	<div style="float: right; width: 100%; text-align: right;">{{ ucfirst(Date::parse($purchase->updated_at)->format('l d \d\e F \d\e Y, h:i:s a.')) }}</div>
	<div style="clear: both; margin: 0pt; padding: 0pt; "></div>
	<br>
	<div>Método de pago: <b>{{ ucwords(mb_strtolower($purchase->payment->key)) }}</b></div>
	<br><br>
	<table>
		<thead>
			<tr>
				<th>Producto</th>
				<th>Cantidad</th>
				<th>Precio</th>
			</tr>
		</thead>
		<tbody>
			@foreach($purchase->inventories as $product)
				<tr>
					<td><small>{{ ucfirst(mb_strtolower($product->inventory->unitType->key)) }} de {{ ucfirst(mb_strtolower($product->inventory->name)) }}</small></td>
					<td>{{ $product->amount }}</td>
					<td>@moneda($product->purchasePrice)</td>
				</tr>
			@endforeach
		</tbody>
		<tfoot>
			<tr>
				<th>Subtotal:</th>
				<th></th>
				<th>@moneda($subtotal)</th>
			</tr>
			<tr>
				<th>Descuento:</th>
				<th></th>
				<th>{{ $purchase->discount }}%</th>
			</tr>
			<tr>
				<th>Total:</th>
				<th></th>
				<th>@moneda($subtotal*(100-$purchase->discount)/100)</th>
			</tr>
		</tfoot>
	</table>
</div>