@extends('adminlte::layouts.app')

@section('htmlheader_title', 'Corte de caja')
@section('contentheader_title','Corte de caja')

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Home</h3>
						<h3 class="box-title pull-right">{{ ucfirst(Date::now()->format('l j \d\e F \d\e Y.')) }}</h3>
					</div>

					<div class="box-body">
						<div class="row">
							<div class="col-md-8 col-md-offset-2"><h2>Resumen de hoy:</h2></div>
							<div class="col-md-8 col-md-offset-2">Número de ventas: {{ $saleAmount }}</div>
						</div>
						@foreach($paymentTypes as $paymentType)
							<div class="row" style="padding-top: 20px;">
								<div class="col-md-2 col-md-offset-2 text-bold text-right">{{ ucfirst(mb_strtolower($paymentType->key)) }}:</div>
								<div class="col-md-2 text-center">No. de ventas: {{ str_pad($paymentType->amount, 4, '0', STR_PAD_LEFT) }}</div>
								<div class="col-md-4">Ingreso: @moneda($paymentType->total)</div>
							</div>
						@endforeach
						<div class="row" style="margin-top: 50px;">
							<div class="col-md-8 col-md-offset-2 text-bold">Ingreso total: @moneda($totalIncome)</div>
							<div class="col-md-12 text-center">
								<a class="btn btn-flat btn-primary" href="{{ url('/cashClosings/report') }}" target="_blank">Generar informe</a>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
@endsection

@section('estilos')
	<style>
	</style>
@endsection

@section('javascripts')
	<script>
		$(function() {

		});
	</script>
@endsection