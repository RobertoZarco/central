@extends('adminlte::layouts.app')

@section('htmlheader_title', 'Contaduria')
@section('contentheader_title','Contaduria')
@section('contentheader_description','DescContaduria')


@section('main-content')
<link rel="stylesheet" href="css/tabla1.css">
<div class="container-fluid spark-screen">
  <div class="row">
   <div class="col-md-9 col-md-offset-1">
    <div class="box box-success box-solid">
        <div class="box-header with-border" id="footertitulo">
            <h3 class="box-title">Contaduria</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered" id="tabla">
              <thead>
                <tr>
                  <th scope="col"></th>
                  <th scope="col">2016</th>
                  <th scope="col">2017</th>
                  <th scope="col">2018</th>
              </tr>
          </thead>
          <tbody>
            <tr>
              <th class="tituloventas" scope="row">Ingreso por ventas</th>
              <td class="tituloventas"></td>
              <td class="tituloventas"></td>
              <td class="tituloventas"></td>
          </tr>
          <tr>
              <th scope="row">Con dinero en efectivo</th>
              <td></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <th scope="row">Con cheque a depositar a banco</th>
              <td></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <th scope="row">Con transferencia bancaria</th>
              <td></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <th scope="row">Con tarjeta de crédito</th>
              <td></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <th scope="row">Con tarjeta de débito</th>
              <td></td>
              <td></td>
              <td></td>
          </tr>
          <tr>
              <th scope="row">Con crédito, se registra un adeudo</th>
              <td></td>
              <td></td>
              <td></td>
          </tr>
      </tbody>
      <tfoot>
        <tr>
          <td class="totales">Totales</td>
          <td></td>
          <td></td>
          <td></td>
      </tr>
  </tfoot>
</table>
<br><br>
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col">2016</th>
      <th scope="col">2017</th>
      <th scope="col">2018</th>
  </tr>
</thead>
<tbody>
    <tr>
      <th class="tituloventas" scope="row">Total de gastos</th>
      <td class="tituloventas"></td>
      <td class="tituloventas"></td>
      <td class="tituloventas"></td>
  </tr>
  <tr>
      <th scope="row">Con dinero en efectivo</th>
      <td></td>
      <td></td>
      <td></td>
  </tr>
  <tr>
      <th scope="row">Con cheque a depositar a banco</th>
      <td></td>
      <td></td>
      <td></td>
  </tr>
  <tr>
      <th scope="row">Con transferencia bancaria</th>
      <td></td>
      <td></td>
      <td></td>
  </tr>
  <tr>
      <th scope="row">Con tarjeta de crédito</th>
      <td></td>
      <td></td>
      <td></td>
  </tr>
  <tr>
      <th scope="row">Con tarjeta de débito</th>
      <td></td>
      <td></td>
      <td></td>
  </tr>
  <tr>
      <th scope="row">Con crédito, se registra un adeudo</th>
      <td></td>
      <td></td>
      <td></td>
  </tr>
</tbody>
<tfoot>
        <tr>
          <td class="totales">Totales</td>
          <td></td>
          <td></td>
          <td></td>
      </tr>
  </tfoot>
</table>


</div>
<!-- /.box-body -->
</div>
</div>
</div>
</div>
@endsection
