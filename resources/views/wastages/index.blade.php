@extends('adminlte::layouts.app')

@section('htmlheader_title', 'Devoluciones')
@section('contentheader_title','Devoluciones')
@section('contentheader_description','Devoluciones')


@section('main-content')

<div class="container-fluid spark-screen">
  <div class="row">
    <div class="col-md-9 col-md-offset-1">
      <div class="box box-primary box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Buscar en compras</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="box-body">
            @if (!empty($success) || session("success"))
              <div class="alert alert-success" role="alert">
                <strong>{{ $success ?? session('success') }}</strong>
              </div>
            @endif
            <div class="col-xs-12">
              <table id="sales" class="table table-bordered table-condensed mi-tabla table-hover table-striped datatable">
                <thead>
                  <tr>
                    <th>Venta</th>
                    <th>Cliente</th>
                    <th>Razon</th>
                    <th>Tipo de Pago</th>
                    <th>Creado</th>
                    <th>PDF</th>
                    <th>Detalle</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($sales as $venta)
                    <tr>
                      <td> {{ $venta->id}}</td>
                      <td> {{ $venta->client->alias }}</td>
                      <td> {{ $venta->client->reason }}</td>
                      <td> {{ $venta->payment->description }}</td>
                      <td> {{ $venta->created_at }}</td>
                      <td>
                        <a href='{{url("/sales/".encrypt($venta->id) )}}' target="_blank" class="btn btn-danger btn-xs" id="btnDetails" data-Id={{$venta->id}}>
                          <span class="fa fa-file-pdf-o"></span>
                        </a>
                      </td>
                      <td>
                        <a href='{{url("/sale/$venta->id" )}}' class="btn btn-primary btn-xs" id="btnDetails" data-Id={{$venta->id}}>
                          <span class="glyphicon glyphicon-eye-open"></span>
                        </a>
                      </td>
                   </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br>

@endsection

@section('javascripts')
  <script src="{{ asset('js/wastage.js') }}"></script>

  <script>
    $(function(){
    })
  </script>

@endsection
