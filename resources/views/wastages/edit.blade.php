@extends('adminlte::page')

@section('htmlheader_title', 'Devoluciones')
@section('contentheader_title','Devoluciones')
@section('contentheader_description','Devoluciones')

@section('htmlheader_title')
	Detalle de venta
@endsection

@section('main-content')

  <link rel="stylesheet" href="{{asset('/css/buttons.css')}}" type="text/css">

	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="box box-primary box-solid">
          <div class="box-header with-border">
            <h3 class="box-title">Venta</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
              <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">

            <table class="table table-responsive table-inverse">
              <thead>
                <tr>
                  <th>Venta</th>
                  <th>Cliente</th>
                  <th>Razon</th>
                  <th>Tipo de pago</th>
                  <th>Descuento</th>
                  <th>Creado</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td> {{ $sale->id}}</td>
                  <td> {{ $sale->client->alias }}</td>
                  <td> {{ $sale->client->reason }}</td>
                  <td> {{ $sale->payment->description }}</td>
                  <td> {{ $sale->discount }}%</td>
                  <td> {{ $sale->created_at }}</td>
                </tr>
              </tbody>
            </table>

          </div>
          <!-- /.box-body -->
        </div>
			</div>
		</div>

    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary box-solid">
          <div class="box-header with-border">
            <h3 class="box-title">Productos</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
              <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">

            <table class="table table-responsive table-inverse">
              <thead>
                <tr>
                  <th>id</th>
                  <th>Nombre</th>
                  <th>Cantidad</th>
                  <th>Precio Venta</th>
                  <th>Subtotal Producto</th>
                  <th>Unidad</th>
                  <th>Categoria</th>
                  <th>Agregar</th>
                </tr>
              </thead>
              <tbody>
                @foreach($sale->inventories as $product)
                  <tr>
                    <td><input type="text" id="id"          name="" value="{{ $product->inventory_id }}"                 title="" class="form-control" disabled=""></td>
                    <td><input type="text" id="name"        name="" value="{{ $product->inventory->name }}"              title="{{ $product->inventory->name }}" class="form-control" disabled=""></td>
                    <td><input type="text" id="amount2"     name="" value="{{ $product->amount }}"                       title="" class="form-control" disabled=""></td>
                    <td><input type="text" id="salePrice"   name="" value="{{ $product->salePrice }}"                    title="" class="form-control" disabled=""></td>
                    <td><input type="text" id="subProducto" name="" value="{{ $product->amount * $product->salePrice }}" title="" class="form-control" disabled=""></td>
                    <td><input type="text" id="unitType"    name="" value="{{ $product->inventory->unitType->key }}"     title="" class="form-control" disabled=""></td>
                    <td><input type="text" id="category"    name="" value="{{ $product->inventory->category->key }}"     title="" class="form-control" disabled=""></td>
                    <td>
                      <button id="btnWastages" data-Id={{$product->inventory_id}} data-amount={{$product->amount}} data-price={{$product->salePrice}} class="btn btn-primary">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                      </button>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary box-solid">
          <div class="box-header with-border">
            <h3 class="box-title">Devolución/Merma</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
              <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="sale" class="table table-responsive table-inverse">
              <thead>
                <tr>
                  <th>id</th>
                  <th>Nombre</th>
                  <th>Cantidad</th>
                  <th>Precio</th>
                  <th>Subtotal</th>
                  <th>Devolución</th>
                  <th>Merma</th>
                  <th>Eliminar</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>



    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="box box-primary ">
          <div class="box-header with-border">
            <h3 class="box-title">Detalle de venta</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-4">
              <div class="form-group">
                <label class="" for="amount">Cantidad</label>
                <input type="text" class="form-control" id="amount" name="amount" placeholder="Cantidad" disabled="">
              </div>

              <div class="form-group">
                <label class="" for="">Subtotal</label>
                <input type="text" class="form-control" id="subTotal" name="subTotal" placeholder="Subtotal" disabled="">
              </div>

              <div class="form-group">
                <label class="" for="">Rebaja</label>
                <input type="text" class="form-control" id="rebaja" name="rebaja" placeholder="Rebaja" min="0" pattern="^[0-9]+" value="{{ $sale->discount }}" disabled="">
              </div>

              <div class="form-group">
                <label class="" for="">Descuento</label>
                <input type="text" class="form-control" id="descuento" name="descuento" placeholder="Total" disabled="">
              </div>

              <div class="form-group">
                <label class="" for="">Total</label>
                <input type="text" class="form-control" id="total" name="total" placeholder="Total" disabled="">
              </div>

              <div class="form-group">
                <label class="" for="">Cliente</label>
                <input type="hidden" id="clientId" value="{{ $sale->client->id }}">
                <input type="text" class="form-control" id="clientName" value="{{ $sale->client->alias }}" disabled="">
              </div>

              <div class="form-group">
                <label class="" for="">Tipo de pago</label>
                <input type="hidden" id="paymentTypeId" value="{{ $sale->payment->id }}">
                <input type="hidden" id="saleId" value="{{ $sale->id }}">
                <input type="text" class="form-control" id="paymentType" value="{{ $sale->payment->description}}" disabled="">
              </div>

              <button type="button"  id="saveWastages" class="btn btn-primary">Guardar</button>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>


    <div class="modal fade" id="modalWastages">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
              </button>
              <h4 class="modal-title">Guardando</h4>
            </div>
            <div class="modal-body">
              <p class="message"></p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" id="accept">Aceptar</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
    </div>



	</div>
@endsection

@section('javascripts')
  <script src="{{ asset('js/wastage.js') }}"></script>

  <script>
    $(function(){
    })
  </script>

@endsection
