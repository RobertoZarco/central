@extends('adminlte::layouts.app')

@section('htmlheader_title', 'Crear Gasto')
@section('contentheader_title','Crear Gasto')

@section('main-content')
<div class="container-fluid spark-screen">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Formulario</h3>
          <h3 class="box-title pull-right">{{ ucfirst(Date::now()->format('l j \d\e F \d\e Y.')) }}</h3>
        </div>

        <div class="box-body row">
          <div class="col-md-6 col-md-offset-3">
            <form method="POST" action="{{ route('expenses.store') }}">
              @csrf
              <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <div class="row flex">
                  <div class="col-md-4">
                    <label class="control-label" for="description">
                      {!! $errors->has('description') ? "<i class='fas fa-times'></i>" : '' !!} Descripcion:
                    </label>
                  </div>

                  <div class="col-md-8">
                    <textarea name="description" id="description" class="form-control" rows="3" required="required">{{ old('description') }}</textarea>
                  </div>

                </div>
                {!! $errors->has('description') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('description')."</span></div>" : '' !!}
              </div>


              <div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
                <div class="row flex">
                  <div class="col-md-4">
                    <label class="control-label" for="amount">
                      {!! $errors->has('amount') ? "<i class='fas fa-times'></i>" : '' !!} Monto:
                    </label>
                  </div>
                  <div class="col-md-8">
                    <input type="text" class="form-control" id="amount" name="amount" value="{{ old('amount') }}">
                  </div>
                </div>
                {!! $errors->has('amount') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('amount')."</span></div>" : '' !!}
              </div>


              <div class="form-group {{ $errors->has('payment_id') ? 'has-error' : '' }}">
                <div class="row flex">
                  <div class="col-md-4">
                    <label class="control-label" for="payment_id">
                      {!! $errors->has('payment_id') ? "<i class='fas fa-times'></i>" : '' !!} Tipo de pago:
                    </label>
                  </div>
                  <div class="col-md-8">
                    <select name="payment_id" id="payment_id" class="select2 form-control" required="required">
                      <option value=""></option>
                      @foreach ($paymentTypes as $paymentType)
                      	@if(old('payment_id') == $paymentType->id)
                        	<option value="{{ $paymentType->id  }}" selected>{{ $paymentType->description }}</option>
                        @else
                        	<option value="{{ $paymentType->id  }}">{{ $paymentType->description }}</option>
                        @endif
                      @endforeach
                    </select>

                  </div>
                </div>
                {!! $errors->has('payment_id') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('payment_id')."</span></div>" : '' !!}
              </div>



              <div class="text-right">
                <a href="{{ url("/expenses") }}" class="pull-left btn btn-flat btn-warning">Regresar</a>
                <button type="submit" class="pull-right btn btn-flat btn-success">Registrar Gasto</button>
              </div>
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
@endsection

@section('estilos')
<style>
div.box{
  padding-bottom: 5rem;
}
div.row.flex {
  display: flex;
  flex-direction: row;
}
div.row.flex > [class^="col-"], div.row.flex > [class*=" col-"] {
  display: flex;
  align-items: center;
  justify-content: right;
}
div.row.flex label.control-label {
  margin: 0;
  text-align: right;
}
label.btn{
  display: inline-table;
  margin-bottom: 10px;
}
label.btn span{
  display: table-cell;
  vertical-align: middle;
}
</style>
@endsection

@section('javascripts')
<script>
  $(function() {

  });
</script>
@endsection