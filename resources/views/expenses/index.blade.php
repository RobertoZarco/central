@extends('adminlte::layouts.app')
@section('htmlheader_title', 'Gastos')
@section('contentheader_title','Gastos')
@section('contentheader_description','listado')

@section('main-content')
<div class="container-fluid spark-screen">
  <div class="row">
    <div class="col-md-9 col-md-offset-1">

      <div class="box box-primary box-solid">
        <div class="box-header with-border">
          <h3 class="box-title">Gastos</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">

          @if(isset($success) || session('success'))

            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <strong>{!! $success ?? session('success') !!}</strong>
            </div>


          @endif

          @if(isset($info) || session('info'))
            <div class="col-md-10 col-md-offset-1 alert alert-info" role="alert">
              <p>{!! $info ?? session('info') !!}</p>
            </div>
          @endif

          <div class="col-md-12 text-center">
            <a href="expenses/create" class="btn btn-flat btn-primary">Nuevo Gasto</a>
          </div>


          <div class="col-md-12">
            @if($expenses->count())
              <table class="datatable table table-condensed table-bordered table-striped table-hover">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Descripción</th>
                    <th>Monto</th>
                    <th>Tipo de pago</th>
                    <th>Creado</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($expenses as $expense)
                    <tr>
                      <td>{{ $expense->id}}</td>
                      <td>{{ $expense->description }}</td>
                      <td>{{ $expense->amount }}</td>
                      <td>{{ $expense->paymentType->description }}</td>
                      <td>{{ $expense->created_at->diffForHumans() }}</td>
                      <td>
                        <a href="{{ action('ExpenseController@edit',$expense->id) }}" class="btn btn-success" data-toggle="tooltip">
                          <i class="fa fa-pencil-square-o"></i>
                        </a>
                      <td>
                        <form method="post" action="{{ action('ExpenseController@destroy',$expense->id) }}">
                          @csrf
                          @method('DELETE')
                          <a href="{{ url()->current()."/$expense->id" }}" class="btn btn-danger" data-toggle="confirmation">
                            <i class="fas fa-trash text-white"></i>
                          </a>
                        </form>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            @else
              <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <strong>No hay registros de gastos</strong>
              </div>
            @endif

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('estilos')
  <style>
  </style>
@endsection

@section('javascripts')
  <script>
    $(function() {
      $('a.btn-success[data-toggle=tooltip]').tooltip({
        container   : 'body',
        placement   : 'left',
        title       : 'Editar',
      });

      $('a.btn-danger[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]',
        container   : 'body',
        title       : 'Eliminar',
        content     : '¿Seguro que deseas eliminar este gasto?',
        popout      : true,
        singleton   : true,
        placement   : 'left',
        onConfirm: function(value) {
          $(this).parent().submit();
        },
        buttons: [
          {
            class :'btn btn-danger',
            icon  :'fa fa-trash',
            label :'Eliminar',
          },{
            class :'btn btn-default',
            icon  :'fa fa-minus-square',
            label :'Cancelar',
            cancel:true,
          },
        ]
      });
      $("table.datatable").DataTable({
        "autoWidth": false,
        order: [],
        scrollX: true,
        columnDefs: [
          {targets: "_all", "className": "text-center"},
          {targets: [-1, -2], "orderable": false},
        ],
        lengthMenu: [[10, 15, 25, 50, -1], [10, 15, 25, 50, "Todos"]],
        language: {
          "lengthMenu"    : "Mostrar _MENU_ gastos por página",
          "zeroRecords"   : "No se encontraron gastos",
          "info"          : "Mostrando página _PAGE_ de _PAGES_",
          "infoEmpty"     : "No hay gastos disponibles",
          "search"        : "Buscar:",
          "infoFiltered"  : "(Filtrados de _MAX_ totales)",
          "loadingRecords": "Cargando...",
          "processing"    : "Procesando...",
          "paginate": {
            "first"   : "Primera",
            "last"    : "Última",
            "next"    : "Siguiente",
            "previous": "Anterior"
          },
        },
        initComplete: function(settings, json){
          $('select[name="DataTables_Table_0_length"]').select2({
            theme: "bootstrap",
            language: "es-MX",
          });
        }
      });
    });
  </script>
@endsection