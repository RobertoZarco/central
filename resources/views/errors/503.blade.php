@extends('adminlte::layouts.errors')

@section('htmlheader_title', 'Error del servidor')

@section('main-content')
	<div class="error-page">
		<h2 class="headline text-blue">503</h2>
		<div class="error-content">
			<h3><i class="fa fa-warning text-blue"></i> Oops! El sistema "{{ config('app.name') }}" se encuentra en mantenimiento.</h3>
			<p>
				El sistema se encuentra en modo mantenimiento, regresaremos pronto!
			</p>
		</div>
	</div><!-- /.error-page -->
@endsection