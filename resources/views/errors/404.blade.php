@extends('adminlte::layouts.errors')

@section('htmlheader_title', 'Página no encontrada')

@section('main-content')

<div class="error-page">
	<h2 class="headline text-yellow"> 404</h2>
	<div class="error-content">
		<h3><i class="fa fa-warning text-yellow"></i> Oops! Página no encontrada.</h3>
		<p>
			No hemos podido encontrar la página que estabas buscando. Mientras tanto, es posible volver al panel.
		</p>
		<a class="btn btn-warning" href="{{ url()->previous() }}">Regresar</a>
	</div><!-- /.error-content -->
</div><!-- /.error-page -->
@endsection