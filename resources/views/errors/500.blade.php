@extends('adminlte::layouts.errors')

@section('htmlheader_title', 'Error del servidor')

@section('main-content')
	<div class="error-page">
		<h2 class="headline text-red">500</h2>
		<div class="error-content">
			<h3><i class="fa fa-warning text-red"></i> Oops! Algo salió mal!</h3>
			<p>
				Algo salió mal al procesar su petición, contacte a su administrador. Mientras tanto, es posible volver al panel.
			</p>
			<a class="btn btn-danger" href="{{ url()->previous() }}">Regresar</a>
		</div>
	</div><!-- /.error-page -->
@endsection