@extends('adminlte::layouts.app')
@section('htmlheader_title', 'Venta/Cotización')
@section('contentheader_title','Venta/Cotización')
@section('contentheader_description','listado')

@section('main-content')
  <link rel="stylesheet" href="{{asset('/css/buttons.css')}}" type="text/css">

  <div class="container-fluid spark-screen">
    <!--tabla inventario-->
    <div class="row">
      <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary ">
          <div class="box-header with-border">
            <h3 class="box-title">Inventario</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="datatable table-bordered table table-responsive table-hover table-condensed table-striped">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Nombre</th>
                  <th>Cantidad</th>
                  {{--<th>Precio de compra</th>--}}
                  <th>Precio de venta</th>
                  <th>unidad</th>
                  <th>categoria</th>
                  <th>Agregar</th>
                </tr>
              </thead>

              <tbody>
                @foreach ($inventories as $inventary)
                  <tr>
                    <td>{{ $inventary->id }}</td>
                    <td>{{ $inventary->name }}</td>
                    <td>{{ $inventary->amount }}</td>
                    {{--<td>${{ $inventary->purchasePrice }}</td>--}}
                    <td>${{ $inventary->salePrice }}</td>
                    <td>{{ $inventary->unitType->description }}</td>
                    <td>{{ $inventary->category->description }}</td>
                    <td>
                      <button type="button" class="btn btn-primary" id="btnAddSale" data-id="{{ $inventary->id }}" data-amount="{{ $inventary->amount }}" data-price="{{ $inventary->salePrice }}" >
                        <span class="glyphicon glyphicon-plus"></span>
                      </button>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>

    <!--productos elegidos-->
    <div class="row">
      <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary ">
          <div class="box-header with-border">
            <h3 class="box-title">Venta/Cotización</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="sale" class="table table-responsive table-hover table-condensed table-striped">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Nombre</th>
                  <th>Cantidad</th>
                  <th>Precio de venta</th>
                  <th>Total Producto</th>
                  <th>Eliminar</th>
                </tr>
              </thead>

              <tbody>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>

    <!--detalle de venta-->
    <div class="row">
      <div class="col-md-9 col-md-offset-1">
        <div class="box box-primary ">
          <div class="box-header with-border">
            <h3 class="box-title">Detalle de venta</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
            @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
            <!-- /.box-tools -->
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-4">
              <div class="form-group">
                <label class="" for="amount">Cantidad</label>
                <input type="number" class="form-control" id="amount" name="amount" placeholder="Cantidad" disabled="">
              </div>

              <div class="form-group">
                <label class="" for="">Subtotal</label>
                <input type="text" class="form-control" id="subTotal" name="subTotal" placeholder="Subtotal" disabled="">
              </div>

              <div class="form-group">
                <label class="" for="">Rebaja</label>
                <input type="number" class="form-control" id="rebaja" name="rebaja" placeholder="Descuento" min="0" pattern="^[0-9]+">
              </div>

              <div class="form-group">
                <label class="" for="">Descuento</label>
                <input type="text" class="form-control" id="descuento" name="descuento" placeholder="Total" disabled="">
              </div>

              <div class="form-group">
                <label class="" for="">Total</label>
                <input type="text" class="form-control" id="total" name="total" placeholder="Total" disabled="">
              </div>

              <div class="form-group">
                <label class="" for="">Cliente</label>
                <select name="client" id="client"  class="form-control" required="required">
                  <option value="" disabled="" id='cliente' selected="">Seleccione cliente</option>
                  @foreach ($clients as $cliente)
                    <option value="{{ $cliente->id }}">{{ $cliente->alias }}</option>
                  @endforeach
                </select>
              </div>

              <div class="form-group">
                <label class="" for="">Tipo de pago</label>
                <select name="payment_id" id="tipoPago" class="form-control" required="required">
                  <option value="" disabled="" selected="">Seleccione Tipo de Pago</option>
                  @foreach ($paymentTypes as $tipoPago)
                    <option value="{{ $tipoPago->id }}">{{ $tipoPago->description }}</option>
                  @endforeach
                </select>
              </div>

              <div class="form-check">
                <label>
                  <input type="radio" name="tipoVenta" id="venta" class='myRadio' value="">
                  <span class="label-text">Venta</span>
                </label>
              </div>

              <div class="form-check">
                <label>
                  <input type="radio" name="tipoVenta" id="cotizacion" value=""  class='myRadio' >
                  <span class="label-text">Cotización</span>
                </label>
              </div>

              <div class="form-group">
                <label class="" for="vencimiento" id="lblVencimiento" style="display: none;" >fecha de vencimiento</label>
                <input type="date" class="form-control" id="vencimiento" name="expire_at" required=""  placeholder="fecha expiración" style="display: none" >
              </div>

              <button type="button" id="saveSale" class="btn btn-primary">Guardar</button>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>

    <div class="modal fade" id="modalVenta">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">Guardando</h4>
          </div>
          <div class="modal-body">
            <p class="message"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="accept">Aceptar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  </div>
@endsection

@section('estilos')
  <style>
  </style>
@endsection

@section('javascripts')
  <script src="{{ asset('js/sales.js') }}"></script>
  <script>
    $(function() {
    });
  </script>
@endsection