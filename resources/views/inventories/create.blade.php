@extends('adminlte::layouts.app')
@section('htmlheader_title', 'Crear producto')

@section('contentheader_title','Inventarios')

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-9 col-md-offset-1">

				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Crear nuevo producto</h3>
					</div>

					<div class="box-body row">
						<div class="col-md-6 col-md-offset-3">
							<form method="post" action="{{ route('inventories.index') }}">
								@csrf

								<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="name">
												{!! $errors->has('name') ? "<i class='fas fa-times'></i>" : '' !!} Nombre:
											</label>
										</div>
										<div class="col-md-8">
											<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
										</div>
									</div>
									{!! $errors->has('name') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('name')."</span></div>" : '' !!}
								</div>
								<div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="amount">
												{!! $errors->has('amount') ? "<i class='fas fa-times'></i>" : '' !!} Cantidad inicial:
											</label>
										</div>
										<div class="col-md-8">
											<input type="text" class="form-control" id="amount" name="amount" value="{{ old('amount') }}">
										</div>
									</div>
									{!! $errors->has('amount') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('amount')."</span></div>" : '' !!}
								</div>
								<div class="form-group {{ $errors->has('purchasePrice') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="purchasePrice">
												{!! $errors->has('purchasePrice') ? "<i class='fas fa-times'></i>" : '' !!} Precio de compra:
											</label>
										</div>
										<div class="col-md-8">
											<input type="text" class="form-control" id="purchasePrice" name="purchasePrice" value="{{ old('purchasePrice') }}">
										</div>
									</div>
									{!! $errors->has('purchasePrice') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('purchasePrice')."</span></div>" : '' !!}
								</div>
								<div class="form-group {{ $errors->has('salePrice') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="salePrice">
												{!! $errors->has('salePrice') ? "<i class='fas fa-times'></i>" : '' !!} Precio de venta:
											</label>
										</div>
										<div class="col-md-8">
											<input type="text" class="form-control" id="salePrice" name="salePrice" value="{{ old('salePrice') }}">
										</div>
									</div>
									{!! $errors->has('salePrice') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('salePrice')."</span></div>" : '' !!}
								</div>
								<div class="form-group {{ $errors->has('unit_id') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="unit_id">
												{!! $errors->has('unit_id') ? "<i class='fas fa-times'></i>" : '' !!} Unidades de medida:
											</label>
										</div>
										<div class="col-md-8">
											<select class="form-control" id="unit_id" name="unit_id">
												@foreach($typesOfUnit as $typeOfUnit)
													@if(old('unit_id') && old('unit_id') == $typeOfUnit->id)
														<option value="{{$typeOfUnit->id}}" selected>{{ ucfirst(mb_strtolower($typeOfUnit->key)) }}</option>
													@else
														<option value="{{$typeOfUnit->id}}">{{ ucfirst(mb_strtolower($typeOfUnit->key)) }}</option>
													@endif
												@endforeach
											</select>
										</div>
									</div>
									{!! $errors->has('unit_id') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('unit_id')."</span></div>" : '' !!}
								</div>
								<div class="form-group {{ $errors->has('category_id') ? 'has-error' : '' }}">
									<div class="row flex">
										<div class="col-md-4">
											<label class="control-label" for="category_id">
												{!! $errors->has('category_id') ? "<i class='fas fa-times'></i>" : '' !!} Categoría:
											</label>
										</div>
										<div class="col-md-8">
											<select class="form-control" id="category_id" name="category_id">
												@foreach($categories as $category)
													@if(old('category_id') && old('category_id') == $category->id)
														<option value="{{$category->id}}" selected>{{ ucfirst(mb_strtolower($category->key)) }}</option>
													@else
														<option value="{{$category->id}}">{{ ucfirst(mb_strtolower($category->key)) }}</option>
													@endif
												@endforeach
											</select>
										</div>
									</div>
									{!! $errors->has('category_id') ? "<div class='row'><span class='help-block col-md-offset-4 col-md-12'>".$errors->first('category_id')."</span></div>" : '' !!}
								</div>
								<div class="text-right">
									<div class="btn-group">
										<a href="{{ url("/inventories") }}" class="btn btn-warning">Regresar</a>
										<button type="submit" class="btn btn-primary">Guardar nuevo producto</button>
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
@endsection

@section('estilos')
	<style>
		div.box{
			padding-bottom: 5rem;
		}
		div.row.flex {
			display: flex;
			flex-direction: row;
		}
		div.row.flex > [class^="col-"], div.row.flex > [class*=" col-"] {
			display: flex;
			align-items: center;
			justify-content: right;
		}
		div.row.flex label.control-label {
			margin: 0;
			text-align: right;
		}
		label.btn{
			display: inline-table;
			margin-bottom: 10px;
		}
		label.btn span{
			display: table-cell;
			vertical-align: middle;
		}
	</style>
@endsection

@section('javascripts')
<script>
	$(function() {
		$('select').select2({
			theme: "bootstrap",
			language: "es-MX",
		});
	});
</script>
@endsection