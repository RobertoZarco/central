@extends('adminlte::layouts.app')
@section('htmlheader_title', 'Inventarios')

@section('contentheader_title','Inventarios')

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-9 col-md-offset-1">

				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Stock de productos activos</h3>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						@if (!empty($success) || session("success"))
							<div class="alert alert-success" role="alert">
								<strong>{!! $success ?? session('success') !!}</strong> 
							</div>
						@endif
						@if (!empty($info) || session("info"))
							<div class="alert alert-info" role="alert">
								<strong>{!! $info ?? session('info') !!}</strong> 
							</div>
						@endif
						<div class="col-md-12">
							<table class="table datatable table-bordered">
								<thead>
									<tr>
										<th>Nombre</th>
										<th>Cantidad</th>
										<th>Precio</th>
										<th>Unidad</th>
										<th>Categoria</th>
										<th>Editar</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($inventory as $producto)
									<tr>
										<td>{{ ucwords(mb_strtolower($producto->name)) }}</td>
										<td>{{ $producto->amount }}</td>
										<td>@moneda($producto->salePrice)</td>
										<td>{{ ucfirst(mb_strtolower($producto->unitType->key)) }}</td>
										<td>{{ ucfirst(mb_strtolower($producto->category->key)) }}</td>
										<td>
											<form method="post" action="{{ url()->current()."/$producto->id" }}">
												@csrf
												@method('DELETE')
												<a href="{{ url()->current()."/$producto->id" }}" class="btn btn-info" data-toggle="tooltip"><i class="fa fa-pencil-square-o"></i></a>
												<a href="#" class="btn btn-danger" data-toggle="confirmation"><i class="fa fa-trash-alt"></i></a>
											</form>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>    
					</div>
					<!-- /.box-body -->
				</div>
			</div>
		</div>
	</div>
@endsection

@section('estilos')
	<style>
		table.datatable td, table.datatable th{
			padding: 5px;
			vertical-align: middle;
		}
		table.datatable, div.dataTables_scrollHeadInner{
			margin: auto;
		}
		.dataTables_wrapper{
			text-align: center;
		}
		.dataTables_wrapper .dataTables_length, .dataTables_wrapper .dataTables_info{
			float: left;
		}
		.dataTables_wrapper .dataTables_filter{
			float: right;
		}
	</style>
@endsection

@section('javascripts')
	<script>
		$(function() {
			$('a.btn-info[data-toggle=tooltip]').tooltip({
				container		: 'body',
				placement		: 'left',
				title				: 'Editar',
			});

			$('a.btn-danger[data-toggle=confirmation]').confirmation({
				rootSelector: '[data-toggle=confirmation]',
				container		: 'body',
				title				: 'Eliminar',
				content			: '¿Seguro que deseas eliminar este producto?',
				popout			: true,
				singleton		: true,
				placement		: 'left',
				onConfirm: function(value) {
					$(this).parent().submit();
				},
				buttons: [
					{
						class	:'btn btn-danger',
						icon	:'fas fa-trash',
						label	:'Eliminar',
					},{
						class	:'btn btn-default',
						icon	:'fas fa-times-circle',
						label	:'Cancelar',
						cancel:true,
					},
				]
			});
			var table = $(".datatable").DataTable({
				dom: 'Blfrtip',
				columnDefs: [
					{targets: [-1], "orderable": false},
				],
				lengthMenu: [[10, 15, 30, -1], [10, 15, 30, "Todos"]],
				buttons: [{
					extend: 'excel',
					text: 'Excel',
					className: 'btn-primary',
					init: function(api, node, config) {
						$(node).removeClass('btn-secondary');
					},
				},{
					extend: 'pdf',
					text: 'PDF',
					className: 'btn-primary',
					init: function(api, node, config) {
						$(node).removeClass('btn-secondary');
					},
				},{
					extend: 'print',
					text: 'Imprimir',
					className: 'btn-primary',
					init: function(api, node, config) {
						$(node).removeClass('btn-secondary');
					},
				},{
					text: 'Crear producto',
					className: 'btn-success',
					action: function ( e, dt, node, config ) {
						document.location.href = '{{ URL::current()."/create" }}';
					}
				}],
				language: {
					"lengthMenu"			: "Ver _MENU_ productos por página",
					"zeroRecords"			: "No se encontraron productos para ese filtro",
					"info"						: "Mostrando página _PAGE_ de _PAGES_",
					"infoEmpty"				: "No hay productos disponibles",
					"search"					: "Buscar:",
					"infoFiltered"		: "(Filtrados de _MAX_ totales)",
					"loadingRecords"	: "Cargando...",
					"processing"			: "Procesando...",
					"paginate": {
						"first"			: "Primera",
						"last"			: "Última",
						"next"			: "Siguiente",
						"previous"	: "Anterior"
					},

				},
				initComplete: function(settings, json){
						$('select[name="DataTables_Table_0_length"]').select2({
							theme: "bootstrap",
							language: "es-MX",
						});
					}

			});
		});
	</script>
@endsection

